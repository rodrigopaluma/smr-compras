const functions = require("firebase-functions");
const nodemailer = require("nodemailer");
const admin = require("firebase-admin");
admin.initializeApp();
const algoliasearch = require("algoliasearch");
const ALGOLIA_ID = "K6JY2F1Y51";
const ALGOLIA_ADMIN_KEY = "a5fb9de4535dbdb5469d63fb3353c429";
const ALGOLIA_SEARCH_KEY = "5834f19706ef77b3ba561937bfbef5c0";

const ALGOLIA_INDEX_NAME = "smr-compras";
const ALGOLIA_INDEX_NAME_PCO = "smr-pco";
const client = algoliasearch(ALGOLIA_ID, ALGOLIA_ADMIN_KEY);

const mailTransport = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: "sistemamalice",
    pass: "Piramide2015"
  }
});

exports.sendEmail = functions.firestore
  .document("solicitacoes/{solicitacaoId}/historico/{historicoId}")
  .onCreate((snapshot, context) => {
    const original = snapshot.data();
    const emails = [];
    if (original.emailSolicitante) {
      emails.push(original.emailSolicitante);
    }
    if (original.emailComprador) {
      emails.push(original.emailComprador);
    }

    const mailOptions = {
      from: `SMR-Compras <smr-compras@smr-compras.com.br>`,
      to: emails.join(", ")
    };

    mailOptions.subject = `SMR-Compras - Alteração na solicitação ${original.numeroSo}`;
    mailOptions.text = `${original.msg}`;
    mailOptions.html = `
    <!doctype html>
<html>
  <head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Software Compras - SMR Automotive</title>
    <style>
      img {
        border: none;
        -ms-interpolation-mode: bicubic;
        max-width: 100%; }
      body {
        background-color: #f6f6f6;
        font-family: sans-serif;
        -webkit-font-smoothing: antialiased;
        font-size: 14px;
        line-height: 1.4;
        margin: 0;
        padding: 0;
        -ms-text-size-adjust: 100%;
        -webkit-text-size-adjust: 100%; }
      table {
        border-collapse: separate;
        mso-table-lspace: 0pt;
        mso-table-rspace: 0pt;
        width: 100%; }
        table td {
          font-family: sans-serif;
          font-size: 14px;
          vertical-align: top; }
      .body {
        background-color: #f6f6f6;
        width: 100%; }
      .container {
        display: block;
        Margin: 0 auto !important;
        max-width: 580px;
        padding: 10px;
        width: 580px; }
      .content {
        box-sizing: border-box;
        display: block;
        Margin: 0 auto;
        max-width: 580px;
        padding: 10px; }
      .main {
        background: #ffffff;
        border-radius: 3px;
        width: 100%; }
      .wrapper {
        box-sizing: border-box;
        padding: 20px; }
      .content-block {
        padding-bottom: 10px;
        padding-top: 10px;
      }
      .footer {
        clear: both;
        Margin-top: 10px;
        text-align: center;
        width: 100%; }
        .footer td,
        .footer p,
        .footer span,
        .footer a {
          color: #999999;
          font-size: 12px;
          text-align: center; }
      h1,
      h2,
      h3,
      h4 {
        color: #000000;
        font-family: sans-serif;
        font-weight: 400;
        line-height: 1.4;
        margin: 0;
        margin-bottom: 30px; }
      h1 {
        font-size: 35px;
        font-weight: 300;
        text-align: center;
        text-transform: capitalize; }
      p,
      ul,
      ol {
        font-family: sans-serif;
        font-size: 14px;
        font-weight: normal;
        margin: 0;
        margin-bottom: 15px; }
        p li,
        ul li,
        ol li {
          list-style-position: inside;
          margin-left: 5px; }
      a {
        color: #3498db;
        text-decoration: underline; }
      .btn {
        box-sizing: border-box;
        width: 100%; }
        .btn > tbody > tr > td {
          padding-bottom: 15px; }
        .btn table {
          width: auto; }
        .btn table td {
          background-color: #ffffff;
          border-radius: 5px;
          text-align: center; }
        .btn a {
          background-color: #ffffff;
          border: solid 1px #3498db;
          border-radius: 5px;
          box-sizing: border-box;
          color: #3498db;
          cursor: pointer;
          display: inline-block;
          font-size: 14px;
          font-weight: bold;
          margin: 0;
          padding: 12px 25px;
          text-decoration: none;
          text-transform: capitalize; }
      .btn-primary table td {
        background-color: #d3040e; }
      .btn-primary a {
        background-color: #d3040e;
        border-color: #d3040e;
        color: #ffffff; }
      .last {
        margin-bottom: 0; }
      .first {
        margin-top: 0; }
      .align-center {
        text-align: center; }
      .align-right {
        text-align: right; }
      .align-left {
        text-align: left; }
      .clear {
        clear: both; }
      .mt0 {
        margin-top: 0; }
      .mb0 {
        margin-bottom: 0; }
      .preheader {
        color: transparent;
        display: none;
        height: 0;
        max-height: 0;
        max-width: 0;
        opacity: 0;
        overflow: hidden;
        mso-hide: all;
        visibility: hidden;
        width: 0; }
      .powered-by a {
        text-decoration: none; }
      hr {
        border: 0;
        border-bottom: 1px solid #f6f6f6;
        Margin: 20px 0; }
      @media only screen and (max-width: 620px) {
        table[class=body] h1 {
          font-size: 28px !important;
          margin-bottom: 10px !important; }
        table[class=body] p,
        table[class=body] ul,
        table[class=body] ol,
        table[class=body] td,
        table[class=body] span,
        table[class=body] a {
          font-size: 16px !important; }
        table[class=body] .wrapper,
        table[class=body] .article {
          padding: 10px !important; }
        table[class=body] .content {
          padding: 0 !important; }
        table[class=body] .container {
          padding: 0 !important;
          width: 100% !important; }
        table[class=body] .main {
          border-left-width: 0 !important;
          border-radius: 0 !important;
          border-right-width: 0 !important; }
        table[class=body] .btn table {
          width: 100% !important; }
        table[class=body] .btn a {
          width: 100% !important; }
        table[class=body] .img-responsive {
          height: auto !important;
          max-width: 100% !important;
          width: auto !important; }}
      @media all {
        .ExternalClass {
          width: 100%; }
        .ExternalClass,
        .ExternalClass p,
        .ExternalClass span,
        .ExternalClass font,
        .ExternalClass td,
        .ExternalClass div {
          line-height: 100%; }
        .apple-link a {
          color: inherit !important;
          font-family: inherit !important;
          font-size: inherit !important;
          font-weight: inherit !important;
          line-height: inherit !important;
          text-decoration: none !important; }
        .btn-primary table td:hover {
          background-color: #34495e !important; }
        .btn-primary a:hover {
          background-color: #34495e !important;
          border-color: #34495e !important; } }
    </style>
  </head>
  <body class="">
    <table border="0" cellpadding="0" cellspacing="0" class="body">
      <tr>
        <td>&nbsp;</td>
        <td class="container">
          <div class="content">
            <span class="preheader">This is preheader text. Some clients will show this text as a preview.</span>
            <table class="main">
              <tr>
                <td class="wrapper">
                  <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                      <td>
                        <img src="https://firebasestorage.googleapis.com/v0/b/smr-compras.appspot.com/o/topo-email-smr-compras.jpg?alt=media&token=7c1a4d8c-2d96-42f2-80da-20ad84365fa4" alt="Software Compras - SMR Automotive" />
                        <p>Olá,</p>
                        <p>Não responda esse e-mail. Este e-mail tem a intenção de informar a atualização do status da sua solicitação.</p>
                        <table border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
                          <tbody>
                            <tr>
                              <td align="center">
                                <table border="0" cellpadding="0" cellspacing="0">
                                  <tbody>
                                    <tr>
                                      <td> <a href="https://smr-compras.firebaseapp.com/solicitacao/${original.id}" target="_blank">Ver Solicitação</a> </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <p>
                            <b>Última atualização da solicitação</b>:<br>
                        </p>
                        <ul style="margin-left:-30px;">
                            <li>${original.msg}</li>
                        </ul>
                        <p> <b> Descrição da solicitação </b>:<br>
                        </p>
                        <ul style="margin-left:-30px;">
                            <li>${original.descricao}</li>
                        </ul>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
            <div class="footer">
              <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td class="content-block">
                    <span class="apple-link">SMR Automotive, Avenida Pacífico Moneda, 3360</span>
                    <br> Capotuna, Jaguariúna - SP
                  </td>
                </tr>
                <tr>
                  <td class="content-block powered-by">
                    Desenvolvido pela <a href="https://www.agenciaondaforte.com.br" target="_blank">Agência Onda Forte</a>.
                  </td>
                </tr>
              </table>
            </div>
          </div>
        </td>
        <td>&nbsp;</td>
      </tr>
    </table>
  </body>
</html>

    `;
    return mailTransport.sendMail(mailOptions).then(() => {
      return console.log("Mensagem enviada");
    });
  });

exports.sendMessage = functions.firestore
  .document("solicitacoes/{solicitacaoId}")
  .onCreate((snapshot, context) => {
    const solicitacao = snapshot.data();

    // ref to the parent document
    const db = admin.firestore();

    const userRef = db.collection("usuarios", query =>
      query.where("role", "==", "administrador")
    );

    // get users tokens and send notifications
    userRef
      .get()
      .then(querySnapshot => {
        if (querySnapshot.size > 0) {
          querySnapshot.forEach(documentSnapshot => {
            const user = documentSnapshot.data();
            const tokens = user.token ? user.token : [];
            if (tokens.length) {
              const payload = {
                notification: {
                  title: `SMR-Compras - Solicitação ${solicitacao.numero}`,
                  body: `Solicitação incluída no sistema SMR-Compras por ${solicitacao.solicitante} - ${solicitacao.departamento}`,
                  icon:
                    "https://res.cloudinary.com/agenciaondaforte/image/upload/v1537460523/logotipo-smr-automotive-compras_ch0z1s.png",
                  click_action: `https://smr-compras.firebaseapp.com/solicitacao/${solicitacao.id}`
                }
              };
              return admin.messaging().sendToDevice(tokens, payload);
            }
          });
        }
      })
      .catch(err => console.log(err));

    return true;
  });

exports.addAlgolia = functions.firestore
  .document("solicitacoes/{solicitacaoId}")
  .onCreate((snapshot, context) => {
    const sol = snapshot.data();
    const db = admin.firestore();

    const solAis = {
      numero: sol.numero,
      solicitante: sol.solicitante,
      departamento: sol.departamento,
      status: sol.alt,
      escopo: sol.escopo,
      objectID: sol.id,
      dataInclusao: sol.inclusao
        .toDate()
        .toISOString()
        .substr(0, 10)
        .split("-")
        .reverse()
        .join("/"),
      dataAlteracao: sol.alteracao
        ? sol.alteracao
            .toDate()
            .toISOString()
            .substr(0, 10)
            .split("-")
            .reverse()
            .join("/")
        : new Date()
            .toISOString()
            .substr(0, 10)
            .split("-")
            .reverse()
            .join("/"),
      tipoMaterial: sol.tipoDeMaterial,
      categoria: sol.material,
      valorInicial: sol.valorInicial,
      valorNegociado: sol.valorNegociado,
      numeroPO: sol.numeroPO,
      comprador: sol.comprador,
      prazo: sol.prazo
        ? sol.prazo
            .toDate()
            .toISOString()
            .substr(0, 10)
            .split("-")
            .reverse()
            .join("/")
        : null,
      urgente: sol.urgente
    };
    const index = client.initIndex(ALGOLIA_INDEX_NAME);
    return index.saveObject(solAis);
  });

exports.updateAlgolia = functions.firestore
  .document("solicitacoes/{solicitacaoId}")
  .onUpdate((snapshot, context) => {
    const sol = snapshot.after.data();
    const db = admin.firestore();

    if (sol.fornecedorEscolhido) {
      const fornecedorRef = db
        .collection("fornecedor")
        .doc(sol.fornecedorEscolhido);
      return fornecedorRef
        .get()
        .then(querySnapshot => {
          if (querySnapshot.exists) {
            const forn = querySnapshot.data();
            const solAis = {
              numero: sol.numero,
              fornecedor: forn.nome,
              solicitante: sol.solicitante,
              departamento: sol.departamento,
              status: sol.alt,
              escopo: sol.escopo,
              objectID: sol.id,
              dataInclusao: sol.inclusao
                .toDate()
                .toISOString()
                .substr(0, 10)
                .split("-")
                .reverse()
                .join("/"),
              dataAlteracao: sol.alteracao
                .toDate()
                .toISOString()
                .substr(0, 10)
                .split("-")
                .reverse()
                .join("/"),
              tipoMaterial: sol.tipoDeMaterial,
              categoria: sol.material,
              valorInicial: sol.valorInicial,
              valorNegociado: sol.valorNegociado,
              numeroPO: sol.numeroPO,
              comprador: sol.comprador,
              prazo: sol.prazo
                ? sol.prazo
                    .toDate()
                    .toISOString()
                    .substr(0, 10)
                    .split("-")
                    .reverse()
                    .join("/")
                : null,
              urgente: sol.urgente
            };
            const index = client.initIndex(ALGOLIA_INDEX_NAME);
            let retorno = index.saveObject(solAis);
            return retorno
              .then(result => {
                console.log(
                  "Sucesso solicitacao com fornecedor" +
                    solAis.objectID +
                    " atualizada"
                );
                return true;
              })
              .catch(error => {
                console.log("Erro ao salvar o Algolia", error);
                return false;
              });
          }
        })
        .catch(err => console.log("Erro ao recuperar o fornecedor", err));
    } else {
      const solAis = {
        numero: sol.numero,
        solicitante: sol.solicitante,
        departamento: sol.departamento,
        status: sol.alt,
        escopo: sol.escopo,
        objectID: sol.id,
        dataInclusao: sol.inclusao
          .toDate()
          .toISOString()
          .substr(0, 10)
          .split("-")
          .reverse()
          .join("/"),
        dataAlteracao: sol.alteracao
          .toDate()
          .toISOString()
          .substr(0, 10)
          .split("-")
          .reverse()
          .join("/"),
        tipoMaterial: sol.tipoDeMaterial,
        categoria: sol.material,
        valorInicial: sol.valorInicial,
        valorNegociado: sol.valorNegociado,
        numeroPO: sol.numeroPO,
        comprador: sol.comprador,
        prazo: sol.prazo
          ? sol.prazo
              .toDate()
              .toISOString()
              .substr(0, 10)
              .split("-")
              .reverse()
              .join("/")
          : null,
        urgente: sol.urgente
      };
      const index = client.initIndex(ALGOLIA_INDEX_NAME);
      let retorno = index.saveObject(solAis);
      return retorno
        .then(result => {
          console.log("Sucesso solicitacao " + solAis.objectID + " atualizada");
          return true;
        })
        .catch(error => {
          console.log("Erro salvando o Algolia sem fornecedor", error);
          return false;
        });
    }
  });

exports.restoreAlgolia = functions.firestore
  .document("teste2/{testeId}")
  .onCreate((snapshot, context) => {
    const db = admin.firestore();
    const solicitacaoRef = db.collection("solicitacoes");
    return solicitacaoRef
      .get()
      .then(querySnapshot => {
        let log = "Solicitaçoes atualizadas ";
        const index = client.initIndex(ALGOLIA_INDEX_NAME);
        let i = 0;
        const toSave = [];
        querySnapshot.forEach(doc => {
          i = i + 1;
          const sol = doc.data();

          const inicio = 1024;
          const fim = 1028;
          const num = Number.parseInt(sol.numero.slice(0, 4));
          if (num >= inicio && num <= fim) {
            log = log + " " + sol.id;
            const solAis = {
              numero: sol.numero,
              solicitante: sol.solicitante,
              departamento: sol.departamento,
              status: sol.alt,
              escopo: sol.escopo,
              objectID: sol.id,
              dataInclusao: sol.inclusao
                .toDate()
                .toISOString()
                .substr(0, 10)
                .split("-")
                .reverse()
                .join("/"),
              dataAlteracao: sol.alteracao
                ? sol.alteracao
                    .toDate()
                    .toISOString()
                    .substr(0, 10)
                    .split("-")
                    .reverse()
                    .join("/")
                : "",
              tipoMaterial: sol.tipoDeMaterial,
              categoria: sol.material,
              valorInicial: sol.valorInicial,
              valorNegociado: sol.valorNegociado,
              numeroPO: sol.numeroPO,
              comprador: sol.comprador,
              prazo: sol.prazo,
              urgente: sol.urgente
            };
            toSave.push(solAis);
          }
        });
        console.log(toSave);
        toSave.map(obj => {
          index.partialUpdateObject(obj, (err, res) => {
            console.log(res, err);
          });
        });
        console.log(log, i + " solicitações revisadas");
      })
      .catch(err => console.log(err));
  });

exports.addAlgoliaPco = functions.firestore
  .document("pco-projetos/{projetoId}")
  .onCreate((snapshot, context) => {
    const projeto = snapshot.data();
    const db = admin.firestore();

    const projetoAis = {
      number: projeto.number,
      projectName: projeto.projectName,
      projectType: projeto.projectType,
      milestone: projeto.phase,
      budget: projeto.budget,
      fromTo: (projeto.currentSupplier ? projeto.currentSupplier : '---') + ' / ' + (projeto.newSupplier ? projeto.newSupplier : '---'),
      user: projeto.user,
      department: projeto.department,
      inclusion: projeto.inclusion,
      modified: projeto.modified,
      saving: projeto.potentialSaving,
      objectID: projeto.id

    };
    const index = client.initIndex(ALGOLIA_INDEX_NAME_PCO);
    return index.saveObject(projetoAis);
  });
