// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBz-Uo90TtdCPORQOs8LmI-P0s6nVTkZf8",
    authDomain: "smr-compras.firebaseapp.com",
    databaseURL: "https://smr-compras.firebaseio.com",
    projectId: "smr-compras",
    storageBucket: "smr-compras.appspot.com",
    messagingSenderId: "235065874264"
  }
};
