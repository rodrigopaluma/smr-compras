import { Component, ViewEncapsulation, ElementRef, OnInit, HostBinding } from '@angular/core';
import { GlobalState } from '../app.state';

import { ConfigService } from '../shared/services/config/config.service';
import { MessagingService } from '../shared/services/messaging/messaging.service';
@Component({
	selector: 'app-layout',
	templateUrl: './layout.component.html',
	styleUrls: ['./layout.component.scss'],
	encapsulation: ViewEncapsulation.Emulated,
})
export class LayoutComponent implements OnInit {
	message;
	constructor(public config: ConfigService, private _elementRef: ElementRef, private _state: GlobalState, private msgService: MessagingService) {

	}

	ngOnInit() {
		this.msgService.getPermission()
		this.msgService.receiveMessage()
		this.msgService.currentMessage.subscribe(message => {
			this.message = message
		})
	}

}
