import { Component, OnInit, Input } from '@angular/core';
import { MessagingService } from '../../shared/services/messaging/messaging.service';

@Component({
  selector: 'app-push-message',
  templateUrl: './push-message.component.html',
  styleUrls: ['./push-message.component.scss']
})
export class PushMessageComponent implements OnInit {

  @Input('message') message: any;

  constructor(private msgService: MessagingService) { }

  ngOnInit() {
  }

  close() {
    this.msgService.deleteMessage();
  }

}
