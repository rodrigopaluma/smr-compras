import { NgModule } from '@angular/core';
import { FornecedoresComponent } from './fornecedores.component';
import { SharedModule } from '../../shared/shared.module';
import { RouterModule } from '@angular/router';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

const ROUTE = [
  { path: 'list', component: FornecedoresComponent }
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(ROUTE),
    NgxDatatableModule
  ],
  declarations: [FornecedoresComponent],
  exports: [FornecedoresComponent]
})
export class FornecedoresModule { }
