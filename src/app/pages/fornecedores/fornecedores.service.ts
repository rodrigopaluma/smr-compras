import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

export interface Fornecedor {
  id: string;
  nome: string;
  email: string;
  telefone: string;
}
@Injectable()
export class FornecedorService {

  constructor(private db: AngularFirestore) { }

  getFornecedores(): Observable<Fornecedor[]> {
    return this.db.collection<Fornecedor>('fornecedor', query => query.orderBy('nome')).valueChanges();
  }

  getFornecedor(id: string): Observable<Fornecedor> {
    return this.db.collection('fornecedor').doc<Fornecedor>(id).valueChanges();
  }

  addFornecedor(fornecedor: Fornecedor): Promise<boolean> {
    fornecedor.id = this.db.createId();
    // tslint:disable-next-line:max-line-length
    return this.db.collection('fornecedor').doc(fornecedor.id).set(fornecedor).then(() => true).catch((error) => { console.log(error); return false});
  }

  updateFornecedor(fornecedor: Fornecedor): Promise<boolean> {
    // tslint:disable-next-line:max-line-length
    return this.db.collection('fornecedor').doc(fornecedor.id).update(fornecedor).then(() => true).catch((error) => { console.log(error); return false});
  }

  removeFornecedor(id: string): Promise<boolean> {
    return this.db.collection('fornecedor').doc(id).delete().then(() => true).catch((error) => { console.log(error); return false});
  }
}
