import { Component, OnInit } from '@angular/core';
import { FornecedorService, Fornecedor } from '../fornecedores/fornecedores.service';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { startWith, map } from 'rxjs/operators';
import { Contrato, ContratosService, ItemContrato } from './contratos.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-contratos',
  templateUrl: './contratos.component.html',
  styleUrls: ['./contratos.component.scss']
})
export class ContratosComponent implements OnInit {

  fornecedores: Fornecedor[] = [];
  contrato: Contrato;
  contratoForm: FormGroup;
  filteredFornecedores;
  render = false;

  constructor(
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private fornecedorService: FornecedorService,
    private service: ContratosService,
    private toastr: ToastrService,
    private router: Router) { }

  ngOnInit() {
    this.fornecedorService.getFornecedores().subscribe(fornecedores => {
      this.fornecedores = fornecedores;
    });
    this.route.params.subscribe(
      params => {
        if (params.id) {
          window.scroll(0, 0);
          this.service.getContrato(params.id).subscribe(contrato => {
            this.contrato = contrato;
            this.contratoForm = this.formBuilder.group({
              id: this.formBuilder.control(this.contrato.id, []),
              fornecedor: this.formBuilder.control(this.contrato.fornecedor, []),
              escopo: this.formBuilder.control(this.contrato.escopo, []),
              validade: this.formBuilder.control(this.contrato.validade, []),
              numeroPO: this.formBuilder.control(this.contrato.numeroPO, []),
              arquivo: this.formBuilder.control(this.contrato.arquivo, []),
              itens: this.formBuilder.array([])
            })
            this.filteredFornecedores = this.contratoForm.get('fornecedor').valueChanges.pipe(
              startWith(''),
              map(value => this._filter(value))
            );
            this.getItens();
            this.render = true;
          })
        } else {
          this.contratoForm = this.formBuilder.group({
            fornecedor: this.formBuilder.control('', []),
            escopo: this.formBuilder.control('', []),
            validade: this.formBuilder.control('', []),
            numeroPO: this.formBuilder.control('', []),
            itens: this.formBuilder.array([this.createNewItem()]),
            arquivo: this.formBuilder.control('', []),
          })
          this.filteredFornecedores = this.contratoForm.get('fornecedor').valueChanges.pipe(
            startWith(''),
            map(value => this._filter(value))
          );
          this.render = true;
        }
      }
    )
  }

  get displayFn() {
    return (val) => {
      const forn: Fornecedor[] = this.fornecedores.filter(f => {
        if (f.id === val) {
          return f;
        }
      })
      return forn.length > 0 ? forn[0].nome : ''
    }
  }

  // Filtro fornecedores
  private _filter(value): Fornecedor[] {
    if (typeof value === 'string') {
      const filterValue = value.toLowerCase();

      return this.fornecedores.filter(option => option.nome.toLowerCase().includes(filterValue));
    }

  }

  createNewItem(): FormGroup {
    return this.formBuilder.group({
      item: this.formBuilder.control(''),
      descricao: this.formBuilder.control('')
    })
  }

  addItem() {
    this.itensFormArray.push(this.formBuilder.group({
      item: this.formBuilder.control(''),
      descricao: this.formBuilder.control('')
    }))
  }

  removeItem(index: number) {
    this.itensFormArray.removeAt(index);
  }

  getItens() {
    this.service.getItensContrato(this.contrato.id).subscribe(itens => {
      itens.map(item => {
        console.log(item)
        this.itensFormArray.push(this.formBuilder.group({
          item: this.formBuilder.control(item.item, []),
          descricao: this.formBuilder.control(item.descricao, []),
          id: this.formBuilder.control(item.id, [])
        }))
      });
    });
  }

  get itensFormArray(): FormArray {
    return this.contratoForm.get('itens') as FormArray;
  }

  salvar() {
    console.log(this.contratoForm)
    if (this.contratoForm.valid ) {
      const contratoValues = this.contratoForm.value;
      console.log(contratoValues)
      this.service.setContrato(contratoValues).then(result => {
        this.toastr.success('Contrato atualizado com sucesso', 'Gravar Contrato').onHidden.subscribe(() => {
          this.router.navigate(['/contratos']);
        });
        if (this.itensFormArray.touched) {
          for (let i = 0; i < this.itensFormArray.length; i++) {
            const item = this.itensFormArray.at(i)
            if (item.touched) {
              const itemValue: ItemContrato = item.value
              itemValue.item = i;
              this.service.setItemContrato(result, itemValue)
            }
          }
        }
      });
    }
  }
}
