import { Component, OnInit } from '@angular/core';
import { ContratosService, Contrato } from './contratos.service';
import { FornecedorService } from '../fornecedores/fornecedores.service';

@Component({
  selector: 'app-contratos-list',
  templateUrl: './contratos-list.component.html',
  styleUrls: ['./contratos-list.component.scss']
})
export class ContratosListComponent implements OnInit {

  contratos: Contrato[]
  role = 'normal'

  constructor(private service: ContratosService, private fornecedorService: FornecedorService) { }

  ngOnInit() {
    const usr = JSON.parse(sessionStorage.getItem('currentUser'))
    this.role = usr.role
    this.service.getContratos().subscribe(contratos => {
      contratos.map(cont => {
        this.getItems(cont);
        if (cont.fornecedor) {
          this.fornecedorService.getFornecedor(cont.fornecedor).subscribe(forn => {
            cont.fornecedorObj = forn
          })
        }
      });
      this.contratos = contratos;
    })
  }

  getItems(contrato: Contrato) {
    this.service.getItensContrato(contrato.id).subscribe(itens => {
      contrato.itens = itens;
    })
  }

  delete(id) {
    this.service.deleteContrato(id)
  }

}
