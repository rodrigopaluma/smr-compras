import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Fornecedor } from '../fornecedores/fornecedores.service';

export interface ItemContrato {
  id: string;
  item: number;
  descricao: string;
}

export interface Contrato {
  id: string;
  escopo: string;
  fornecedor: string;
  numeroPO: string;
  validade: Date;
  itens?: ItemContrato[];
  arquivo?: any;
  fornecedorObj?: Fornecedor;
}

@Injectable()
export class ContratosService {

  constructor(private db: AngularFirestore) { }

  getContratos(): Observable<Contrato[]> {
    return this.db.collection<Contrato>('contratos').valueChanges();
  }

  getContrato(id: string): Observable<Contrato> {
    return this.db.collection('contratos').doc<Contrato>(id).valueChanges();
  }

  getItensContrato(contratoId){
    return this.db.collection<ItemContrato>(`contratos/${contratoId}/itens`, query => query.orderBy('item', 'asc')).valueChanges();
  }

  setContrato(contrato: Contrato) {
    if (!contrato.id) {
      contrato.id = this.db.createId();
    }
    if (contrato.fornecedorObj) {
      delete contrato.fornecedorObj
    }
    if (contrato.itens) {
      delete contrato.itens
    }
    return this.db.collection<Contrato>(`contratos`).doc(contrato.id).set(contrato).then(result => {
      return contrato.id
    });
  }

  setItemContrato(contratoId: string, item: ItemContrato) {
    if (!item.id) {
      item.id = this.db.createId();
    }
    return this.db.collection<ItemContrato>(`contratos/${contratoId}/itens`).doc(item.id).set(item)
  }

  deleteContrato(contratoId: string) {
    return this.db.doc(`contratos/${contratoId}`).delete();
  }
}
