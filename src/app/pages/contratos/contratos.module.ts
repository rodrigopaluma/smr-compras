import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContratosComponent } from './contratos.component';
import { ContratosListComponent } from './contratos-list.component';
import {MatExpansionModule} from '@angular/material/expansion';
import { SharedModule } from 'app/shared/shared.module';
import { RouterModule } from '@angular/router';

const CONTRATO_ROUTE = [
  { path: '', component: ContratosListComponent },
  { path: 'novo', component: ContratosComponent },
  { path: ':id', component: ContratosComponent },
];

@NgModule({
  declarations: [ContratosComponent, ContratosListComponent],
  imports: [
    CommonModule,
    MatExpansionModule,
    SharedModule,
    RouterModule.forChild(CONTRATO_ROUTE),
  ]
})
export class ContratosModule { }
