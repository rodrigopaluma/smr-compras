import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
// tslint:disable-next-line:import-blacklist
import { Observable } from 'rxjs';

export interface Cliente {
  id: string,
  nome: string;
  programas: any;
}
@Injectable()
export class ClienteService {

  constructor(private db: AngularFirestore) { }

  getClientes(): Observable<Cliente[]> {
    return this.db.collection<Cliente>('clientes').valueChanges();
  }

  getCliente(id: string): Observable<Cliente> {
    return this.db.collection('clientes').doc<Cliente>(id).valueChanges();
  }

  getProgramas(nome: string): Observable<Cliente[]> {
    return this.db.collection<Cliente>('clientes', query => query.where('nome', '==', nome)).valueChanges();
  }

  addCliente(cliente: Cliente): Promise<boolean> {
    cliente.id = this.db.createId();
    // tslint:disable-next-line:max-line-length
    return this.db.collection('clientes').doc(cliente.id).set(cliente).then(() => true).catch((error) => { console.log(error); return false});
  }

  updateCliente(cliente: Cliente): Promise<boolean> {
    // tslint:disable-next-line:max-line-length
    return this.db.collection('clientes').doc(cliente.id).update(cliente).then(() => true).catch((error) => { console.log(error); return false});
  }

  removeCliente(id: string): Promise<boolean> {
    return this.db.collection('clientes').doc(id).delete().then(() => true).catch((error) => { console.log(error); return false});
  }
}
