import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ClienteService } from './cliente.service';
import { Programa, ProgramaService } from '../programa/programa.service'
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.scss']
})
export class ClienteComponent implements OnInit {

  rows = [];
  selected = [];
  temp = [];
  searchValue: string = null;
  isSearchActive = false;
  isToolbarActive = false;
  itemsSelected = '';
  itemCount = 0;
  editing = {};
  programas: Programa[];
  programaTmp;

  constructor(
    private service: ClienteService,
    private toast: ToastrService,
    private programaService: ProgramaService
  ) { }

  ngOnInit() {
    this.service.getClientes().subscribe(clientes => {
      this.rows = clientes;
    });
    this.programaService.getProgramas().subscribe(programas => {
      this.programas = programas;
    })

  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    const temp = this.temp.filter(function (d) {
      return d.productTitle.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.rows = temp;
  }

  onSelect({ selected }) {
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
    if (selected.length === 1) {
      this.isToolbarActive = true;
      this.itemCount = selected.length;
      this.itemsSelected = 'Item Selected';
    } else if (selected.length > 0) {
      this.isToolbarActive = true;
      this.itemCount = selected.length;
      this.itemsSelected = 'Items Selected';
    } else {
      this.isToolbarActive = false;
    }
  }

  triggerClose(event) {
    this.rows = this.temp;
    this.searchValue = '';
    this.isSearchActive = !this.isSearchActive;
  }

  onActivate(event) {

  }

  add() {
    this.rows.push({});
    this.editing[(this.rows.length - 1) + '-nome'] = true;
    this.editing[(this.rows.length - 1) + '-programas'] = true;
    this.rows = [...this.rows];
  }

  ajuste = (a, b) => {
    // set default
    console.log(a, b);
    if (this.editing) { return 250};
  ​
    // return my height
    return 100;
  }


  remove() {
    this.selected.map(obj => {
      this.service.removeCliente(obj.id).then(() => {
        this.toast.success('Cliente excluído com sucesso', 'Sucesso');
      });
    });
    this.isToolbarActive = false;
    this.selected = [];
  }

  updateValue(event, cell, rowIndex) {
    console.log('inline editing rowIndex', rowIndex)
    this.editing[rowIndex + '-' + cell] = false;
    this.rows[rowIndex][cell] = event.target.value;
    const cliente = this.rows[rowIndex];
    if (cliente.id) {
      this.service.updateCliente(cliente).then(() => {
        this.toast.success('Cliente atualizado com sucesso.', 'Sucesso');
      });
    } else {
      if (cliente.nome) {
        this.service.addCliente(cliente).then(() => {
          this.toast.success('Cliente incluído com sucesso.', 'Sucesso');
        });
      }
    }
    this.rows = [...this.rows];
    console.log('UPDATED!', this.rows[rowIndex][cell]);
  }

  updateProgram(index, newValue) {
    this.editing[index + '-programas'] = false;
    const cliente = this.rows[index];
    if (cliente.id) {
      this.service.updateCliente(cliente).then(() => {
        this.toast.success('Cliente atualizado com sucesso.', 'Sucesso');
      });
    } else {
      if (cliente.nome) {
        this.service.addCliente(cliente).then(() => {
          this.toast.success('Cliente incluído com sucesso.', 'Sucesso');
        });
      }
    }
    this.rows = [...this.rows];
  }

}
