import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { RouterModule } from '@angular/router';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ClienteComponent } from './cliente.component';
import { NgSelectModule } from '@ng-select/ng-select';

const ROUTE = [
  { path: '', component: ClienteComponent }
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(ROUTE),
    NgxDatatableModule,
    NgSelectModule
  ],
  declarations: [ClienteComponent],
  exports: [ClienteComponent]
})
export class ClienteModule { }
