import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { RouterModule } from '@angular/router';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { CategoriaDoBudgetComponent } from './categoria-do-budget.component';

const ROUTE = [
  { path: '', component: CategoriaDoBudgetComponent }
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(ROUTE),
    NgxDatatableModule
  ],
  declarations: [CategoriaDoBudgetComponent],
  exports: [CategoriaDoBudgetComponent]
})
export class CategoriaDoBudgetModule { }
