import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

export interface Categoria {
  id: string,
  nome: string;
}
@Injectable()
export class CategoriaDoBudgetService {

  constructor(private db: AngularFirestore) { }

  getCategorias(): Observable<Categoria[]> {
    return this.db.collection<Categoria>('categorias').valueChanges();
  }

  getCategoria(id: string): Observable<Categoria> {
    return this.db.collection('categorias').doc<Categoria>(id).valueChanges();
  }

  addCategoria(categoria: Categoria): Promise<boolean> {
    categoria.id = this.db.createId();
    // tslint:disable-next-line:max-line-length
    return this.db.collection('categorias').doc(categoria.id).set(categoria).then(() => true).catch((error) => { console.log(error); return false});
  }

  updateCategoria(categoria: Categoria): Promise<boolean> {
    // tslint:disable-next-line:max-line-length
    return this.db.collection('categorias').doc(categoria.id).update(categoria).then(() => true).catch((error) => { console.log(error); return false});
  }

  removeCategoria(id: string): Promise<boolean> {
    return this.db.collection('categorias').doc(id).delete().then(() => true).catch((error) => { console.log(error); return false});
  }
}
