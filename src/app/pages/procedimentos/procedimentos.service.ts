import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

export interface Procedimento {
  id: string,
  nome: string;
  arquivo: string;
}

@Injectable()
export class ProcedimentosService {

  constructor(private db: AngularFirestore) { }

  getProcedimentos(): Observable<Procedimento[]> {
    return this.db.collection<Procedimento>('procedimentos').valueChanges();
  }

  getProcedimento(id: string): Observable<Procedimento> {
    return this.db.collection('procedimentos').doc<Procedimento>(id).valueChanges();
  }

  addProcedimento(procedimento: Procedimento): Promise<boolean> {
    procedimento.id = this.db.createId();
    return this.db.collection('procedimentos').doc(procedimento.id).set(procedimento).then(() => true).catch((error) => { console.log(error); return false});
  }

  updateProcedimento(procedimento: Procedimento): Promise<boolean> {
    return this.db.collection('procedimentos').doc(procedimento.id).update(procedimento).then(() => true).catch((error) => { console.log(error); return false});
  }

  removeProcedimento(id: string): Promise<boolean> {
    return this.db.collection('procedimentos').doc(id).delete().then(() => true).catch((error) => { console.log(error); return false});
  }
}
