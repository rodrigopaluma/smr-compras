import { Component, OnInit } from '@angular/core';
import { ProcedimentosService } from './procedimentos.service';
import { ToastrService } from '../../../../node_modules/ngx-toastr';
import { AngularFireStorage } from '@angular/fire/storage';

@Component({
  selector: 'app-procedimentos',
  templateUrl: './procedimentos.component.html',
  styleUrls: ['./procedimentos.component.scss']
})
export class ProcedimentosComponent implements OnInit {

  rows = [];
  selected = [];
  temp = [];
  searchValue: string = null;
  isSearchActive = false;
  isToolbarActive = false;
  itemsSelected = '';
  itemCount = 0;
  editing = {};

  constructor(
    private service: ProcedimentosService,
    private toast: ToastrService,
    private storage: AngularFireStorage
  ) { }

  ngOnInit() {
    this.service.getProcedimentos().subscribe(procedimentos => {
      this.rows = procedimentos;
    });
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    const temp = this.temp.filter(function (d) {
      return d.productTitle.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.rows = temp;
  }

  onSelect({ selected }) {
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
    if (selected.length === 1) {
      this.isToolbarActive = true;
      this.itemCount = selected.length;
      this.itemsSelected = 'Item Selected';
    } else if (selected.length > 0) {
      this.isToolbarActive = true;
      this.itemCount = selected.length;
      this.itemsSelected = 'Items Selected';
    } else {
      this.isToolbarActive = false;
    }
  }

  triggerClose(event) {
    this.rows = this.temp;
    this.searchValue = '';
    this.isSearchActive = !this.isSearchActive;
  }

  onActivate(event) {

  }

  add() {
    this.rows.push({});
    this.editing[(this.rows.length - 1) + '-nome'] = true;
    this.editing[(this.rows.length - 1) + '-arquivo'] = true;
    this.rows = [...this.rows];
  }


  remove() {
    this.selected.map(obj => {
      this.service.removeProcedimento(obj.id).then(() => {
        this.toast.success('Procedimento excluído com sucesso', 'Sucesso');
      });
    });
    this.isToolbarActive = false;
    this.selected = [];
  }

  updateValue(event, cell, rowIndex) {
    console.log('inline editing rowIndex', rowIndex)
    this.editing[rowIndex + '-' + cell] = false;
    this.rows[rowIndex][cell] = event.target.value;
    const procedimento = this.rows[rowIndex];
    if (procedimento.id) {
      this.service.updateProcedimento(procedimento).then(() => {
        this.toast.success('Procedimento atualizado com sucesso.', 'Sucesso');
      });
    } else {
      if (procedimento.nome && procedimento.arquivo) {
        this.service.addProcedimento(procedimento).then(() => {
          this.toast.success('Procedimento incluído com sucesso.', 'Sucesso');
        });
      }
    }
    this.rows = [...this.rows];
    console.log('UPDATED!', this.rows[rowIndex][cell]);
  }

  handleFileInput(file: File, index: number) {
    const randomId = Math.random().toString(36).substring(2);
    const ref = this.storage.ref(randomId);
    const task = ref.put(file)
      .then(t => {
        t.ref.getDownloadURL().then(url => {
          console.log(this.rows, index);
          this.rows[index].arquivo = url;
        });
      })
      .catch(error => console.log(error));
  }

}
