import { NgModule } from '@angular/core';
import { ProcedimentosComponent } from './procedimentos.component';
import { SharedModule } from '../../shared/shared.module';
import { RouterModule } from '@angular/router';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

const ROUTE = [
    { path: 'list', component: ProcedimentosComponent },
  ];

  @NgModule({
    imports: [
      SharedModule,
      RouterModule.forChild(ROUTE),
      NgxDatatableModule
    ],
    declarations: [ProcedimentosComponent],
    exports: [ProcedimentosComponent]
  })
  export class ProcedimentosModule { }
  