import { Component, OnInit } from '@angular/core';
import { DepartamentosService } from './departamentos.service';
import { ToastrService } from '../../../../node_modules/ngx-toastr';

@Component({
  selector: 'app-departamentos',
  templateUrl: './departamentos.component.html',
  styleUrls: ['./departamentos.component.scss']
})
export class DepartamentosComponent implements OnInit {


  rows = [];
  selected = [];
  temp = [];
  searchValue: string = null;
  isSearchActive = false;
  isToolbarActive = false;
  itemsSelected = '';
  itemCount = 0;
  editing = {};


  constructor(
    private service: DepartamentosService,
    private toast: ToastrService
  ) { }

  ngOnInit() {
    this.service.getDepartamentos().subscribe(departamentos => {
      this.rows = departamentos;
    });
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    const temp = this.temp.filter(function (d) {
      return d.productTitle.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.rows = temp;
  }

  onSelect({ selected }) {
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
    if (selected.length === 1) {
      this.isToolbarActive = true;
      this.itemCount = selected.length;
      this.itemsSelected = 'Item Selected';
    } else if (selected.length > 0) {
      this.isToolbarActive = true;
      this.itemCount = selected.length;
      this.itemsSelected = 'Items Selected';
    } else {
      this.isToolbarActive = false;
    }
  }

  triggerClose(event) {
    this.rows = this.temp;
    this.searchValue = '';
    this.isSearchActive = !this.isSearchActive;
  }

  onActivate(event) {

  }

  add() {
    this.rows.push({});
    this.editing[(this.rows.length - 1) + '-nome'] = true;
    this.rows = [...this.rows];
  }


  remove() {
    this.selected.map(obj => {
      this.service.removeDepartamento(obj.id).then(() => {
        this.toast.success('Departamento excluído com sucesso', 'Sucesso');
      });
    });
    this.isToolbarActive = false;
    this.selected = [];
  }

  updateValue(event, cell, rowIndex) {
    console.log('inline editing rowIndex', rowIndex)
    this.editing[rowIndex + '-' + cell] = false;
    this.rows[rowIndex][cell] = event.target.value;
    const departamento = this.rows[rowIndex];
    if (departamento.id) {
      this.service.updateDepartamento(departamento).then(() => {
        this.toast.success('Departamento atualizado com sucesso.', 'Sucesso');
      });
    } else {
      if (departamento.nome) {
        this.service.addDepartamento(departamento).then(() => {
          this.toast.success('Departamento incluído com sucesso.', 'Sucesso');
        });
      }
    }
    this.rows = [...this.rows];
    console.log('UPDATED!', this.rows[rowIndex][cell]);
  }

}
