import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { RouterModule } from '@angular/router';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { DepartamentosComponent } from './departamentos.component';

const ROUTE = [
  { path: 'list', component: DepartamentosComponent }
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(ROUTE),
    NgxDatatableModule
  ],
  declarations: [DepartamentosComponent],
  exports: [DepartamentosComponent]
})
export class DepartamentosModule { }
