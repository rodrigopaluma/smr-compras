import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

export interface Departamento {
  id: string,
  nome: string;
}
@Injectable()
export class DepartamentosService {

  constructor(private db: AngularFirestore) { }

  getDepartamentos(): Observable<Departamento[]> {
    return this.db.collection<Departamento>('departamentos').valueChanges();
  }

  getDepartamento(id: string): Observable<Departamento> {
    return this.db.collection('deparatamentos').doc<Departamento>(id).valueChanges();
  }

  addDepartamento(departamento: Departamento): Promise<boolean> {
    departamento.id = this.db.createId();
    // tslint:disable-next-line:max-line-length
    return this.db.collection('departamentos').doc(departamento.id).set(departamento).then(() => true).catch((error) => { console.log(error); return false});
  }

  updateDepartamento(departamento: Departamento): Promise<boolean> {
    // tslint:disable-next-line:max-line-length
    return this.db.collection('departamentos').doc(departamento.id).update(departamento).then(() => true).catch((error) => { console.log(error); return false});
  }

  removeDepartamento(id: string): Promise<boolean> {
    return this.db.collection('departamentos').doc(id).delete().then(() => true).catch((error) => { console.log(error); return false});
  }
}
