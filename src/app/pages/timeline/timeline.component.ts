import { Component, OnInit } from "@angular/core";
import "rxjs/add/operator/startWith";
import "rxjs/add/operator/map";
import { ActivatedRoute } from "@angular/router";
import { SolicitacaoService, Historico } from "app/pages/solicitacao/solicitacao.service";

@Component({
  selector: ".content_inner_wrapper",
  templateUrl: "./timeline.component.html",
  styleUrls: ["./timeline.component.scss"]
})
export class TimelineComponent implements OnInit {
  //Header Title
  title: string = "Timeline";
  historico: Historico[] = [];
  solicitacaoId = '';

  //Date Picker
  constructor(private route: ActivatedRoute, private service: SolicitacaoService) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      if (params.id) {
        this.solicitacaoId = params.id;
        this.service.getHistorico(params.id).subscribe(h => {
          this.historico = h;
          console.log(h);
        });
      }
    });
  }
}
