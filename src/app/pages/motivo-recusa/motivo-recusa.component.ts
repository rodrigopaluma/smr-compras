import { Component, OnInit } from '@angular/core';
import { MotivoRecusaService } from './motivo-recusa.service';
import { ToastrService } from '../../../../node_modules/ngx-toastr';

@Component({
  selector: 'app-motivo-recusa',
  templateUrl: './motivo-recusa.component.html',
  styleUrls: ['./motivo-recusa.component.scss']
})
export class MotivoRecusaComponent implements OnInit {

  rows = [];
  selected = [];
  temp = [];
  searchValue: string = null;
  isSearchActive = false;
  isToolbarActive = false;
  itemsSelected = '';
  itemCount = 0;
  editing = {};


  constructor(
    private service: MotivoRecusaService,
    private toast: ToastrService
  ) { }

  ngOnInit() {
    this.service.getMotivosRecusa().subscribe(motivosRecusa => {
      this.rows = motivosRecusa;
    });
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    const temp = this.temp.filter(function (d) {
      return d.productTitle.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.rows = temp;
  }

  onSelect({ selected }) {
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
    if (selected.length === 1) {
      this.isToolbarActive = true;
      this.itemCount = selected.length;
      this.itemsSelected = 'Item Selected';
    } else if (selected.length > 0) {
      this.isToolbarActive = true;
      this.itemCount = selected.length;
      this.itemsSelected = 'Items Selected';
    } else {
      this.isToolbarActive = false;
    }
  }

  triggerClose(event) {
    this.rows = this.temp;
    this.searchValue = '';
    this.isSearchActive = !this.isSearchActive;
  }

  onActivate(event) {

  }

  add() {
    this.rows.push({});
    this.editing[(this.rows.length - 1) + '-descricao'] = true;
    this.rows = [...this.rows];
  }


  remove() {
    this.selected.map(obj => {
      this.service.removeMotivoRecusa(obj.id).then(() => {
        this.toast.success('Motivo excluído com sucesso', 'Sucesso');
      });
    });
    this.isToolbarActive = false;
    this.selected = [];
  }

  updateValue(event, cell, rowIndex) {
    console.log('inline editing rowIndex', rowIndex)
    this.editing[rowIndex + '-' + cell] = false;
    this.rows[rowIndex][cell] = event.target.value;
    const motivo = this.rows[rowIndex];
    if (motivo.id) {
      this.service.updateMotivoRecusa(motivo).then(() => {
        this.toast.success('Motivo atualizado com sucesso.', 'Sucesso');
      });
    } else {
      if (motivo.descricao) {
        this.service.addMotivoRecusa(motivo).then(() => {
          this.toast.success('Motivo incluído com sucesso.', 'Sucesso');
        });
      }
    }
    this.rows = [...this.rows];
    console.log('UPDATED!', this.rows[rowIndex][cell]);
  }

}
