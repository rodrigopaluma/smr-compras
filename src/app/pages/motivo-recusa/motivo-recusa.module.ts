import { NgModule } from '@angular/core';
import { MotivoRecusaComponent } from './motivo-recusa.component';
import { SharedModule } from '../../shared/shared.module';
import { RouterModule } from '@angular/router';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

const ROUTE = [
  { path: '', component: MotivoRecusaComponent },
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(ROUTE),
    NgxDatatableModule
  ],
  declarations: [MotivoRecusaComponent]
})
export class MotivoRecusaModule { }
