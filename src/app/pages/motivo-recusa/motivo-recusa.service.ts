import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

export interface MotivoRecusa {
  id: string;
  descricao: string;
}
@Injectable()
export class MotivoRecusaService {

  constructor(private db: AngularFirestore) { }

  getMotivosRecusa(): Observable<MotivoRecusa[]> {
    return this.db.collection<MotivoRecusa>('motivo_recusa').valueChanges();
  }

  getMotivoRecusa(id: string): Observable<MotivoRecusa> {
    return this.db.collection('motivo_recusa').doc<MotivoRecusa>(id).valueChanges();
  }

  addMotivoRecusa(moeda: MotivoRecusa): Promise<boolean> {
    moeda.id = this.db.createId();
    // tslint:disable-next-line:max-line-length
    return this.db.collection('motivo_recusa').doc(moeda.id).set(moeda).then(() => true).catch((error) => { console.log(error); return false});
  }

  updateMotivoRecusa(moeda: MotivoRecusa): Promise<boolean> {
    // tslint:disable-next-line:max-line-length
    return this.db.collection('motivo_recusa').doc(moeda.id).update(moeda).then(() => true).catch((error) => { console.log(error); return false});
  }

  removeMotivoRecusa(id: string): Promise<boolean> {
    return this.db.collection('motivo_recusa').doc(id).delete().then(() => true).catch((error) => { console.log(error); return false});
  }
}
