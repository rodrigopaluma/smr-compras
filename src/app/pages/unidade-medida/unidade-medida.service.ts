import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs/Observable';

export interface UnidadeMedida {
  id: string;
  tipo: string;
  descricao: string;
}
@Injectable()
export class UnidadeMedidaService {

  constructor(private db: AngularFirestore) { }

  getUnidadesMedida(): Observable<UnidadeMedida[]> {
    return this.db.collection<UnidadeMedida>('unidade_medida').valueChanges();
  }

  getUnidadeMedida(id: string): Observable<UnidadeMedida> {
    return this.db.collection('unidade_medida').doc<UnidadeMedida>(id).valueChanges();
  }

  addUnidadeMedida(unidadeMedida: UnidadeMedida): Promise<boolean> {
    unidadeMedida.id = this.db.createId();
    // tslint:disable-next-line:max-line-length
    return this.db.collection('unidade_medida').doc(unidadeMedida.id).set(unidadeMedida).then(() => true).catch((error) => { console.log(error); return false});
  }

  updateUnidadeMedida(unidadeMedida: UnidadeMedida): Promise<boolean> {
    // tslint:disable-next-line:max-line-length
    return this.db.collection('unidade_medida').doc(unidadeMedida.id).update(unidadeMedida).then(() => true).catch((error) => { console.log(error); return false});
  }

  removeUnidadeMedida(id: string): Promise<boolean> {
    return this.db.collection('unidade_medida').doc(id).delete().then(() => true).catch((error) => { console.log(error); return false});
  }
}
