import { NgModule } from '@angular/core';
import { UnidadeMedidaComponent } from './unidade-medida.component';
import { SharedModule } from '../../shared/shared.module';
import { RouterModule } from '@angular/router';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

const ROUTE = [
  { path: '', component: UnidadeMedidaComponent },
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(ROUTE),
    NgxDatatableModule
  ],
  declarations: [UnidadeMedidaComponent]
})
export class UnidadeMedidaModule { }
