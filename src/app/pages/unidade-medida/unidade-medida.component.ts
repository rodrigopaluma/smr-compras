import { Component, OnInit } from '@angular/core';
import { UnidadeMedidaService } from './unidade-medida.service';
import { ToastrService } from '../../../../node_modules/ngx-toastr';

@Component({
  selector: 'app-unidade-medida',
  templateUrl: './unidade-medida.component.html',
  styleUrls: ['./unidade-medida.component.scss']
})
export class UnidadeMedidaComponent implements OnInit {

  rows = [];
  selected = [];
  temp = [];
  searchValue: string = null;
  isSearchActive = false;
  isToolbarActive = false;
  itemsSelected = '';
  itemCount = 0;
  editing = {};


  constructor(
    private service: UnidadeMedidaService,
    private toast: ToastrService
  ) { }

  ngOnInit() {
    this.service.getUnidadesMedida().subscribe(unidadesMedida => {
      this.rows = unidadesMedida;
    });
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    const temp = this.temp.filter(function (d) {
      return d.productTitle.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.rows = temp;
  }

  onSelect({ selected }) {
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
    if (selected.length === 1) {
      this.isToolbarActive = true;
      this.itemCount = selected.length;
      this.itemsSelected = 'Item Selected';
    } else if (selected.length > 0) {
      this.isToolbarActive = true;
      this.itemCount = selected.length;
      this.itemsSelected = 'Items Selected';
    } else {
      this.isToolbarActive = false;
    }
  }

  triggerClose(event) {
    this.rows = this.temp;
    this.searchValue = '';
    this.isSearchActive = !this.isSearchActive;
  }

  onActivate(event) {

  }

  add() {
    this.rows.push({});
    this.editing[(this.rows.length - 1) + '-tipo'] = true;
    this.editing[(this.rows.length - 1) + '-descricao'] = true;
    this.rows = [...this.rows];
  }


  remove() {
    this.selected.map(obj => {
      this.service.removeUnidadeMedida(obj.id).then(() => {
        this.toast.success('Unidade de medida excluída com sucesso', 'Sucesso');
      });
    });
    this.isToolbarActive = false;
    this.selected = [];
  }

  updateValue(event, cell, rowIndex) {
    console.log('inline editing rowIndex', rowIndex)
    this.editing[rowIndex + '-' + cell] = false;
    this.rows[rowIndex][cell] = event.target.value;
    const medida = this.rows[rowIndex];
    if (medida.id) {
      this.service.updateUnidadeMedida(medida).then(() => {
        this.toast.success('Unidade de medida atualizada com sucesso.', 'Sucesso');
      });
    } else {
      if (medida.tipo && medida.descricao) {
        this.service.addUnidadeMedida(medida).then(() => {
          this.toast.success('Unidade de medida incluída com sucesso.', 'Sucesso');
        });
      }
    }
    this.rows = [...this.rows];
    console.log('UPDATED!', this.rows[rowIndex][cell]);
  }

}
