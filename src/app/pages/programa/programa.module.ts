import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { RouterModule } from '@angular/router';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ProgramaComponent } from './programa.component';

const ROUTE = [
  { path: '', component: ProgramaComponent }
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(ROUTE),
    NgxDatatableModule
  ],
  declarations: [ProgramaComponent],
  exports: [ProgramaComponent]
})
export class ProgramaModule { }
