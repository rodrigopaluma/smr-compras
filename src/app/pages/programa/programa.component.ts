import { Component, OnInit } from '@angular/core';
import { ProgramaService } from './programa.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-programa',
  templateUrl: './programa.component.html',
  styleUrls: ['./programa.component.scss']
})
export class ProgramaComponent implements OnInit {

  rows = [];
  selected = [];
  temp = [];
  searchValue: string = null;
  isSearchActive = false;
  isToolbarActive = false;
  itemsSelected = '';
  itemCount = 0;
  editing = {};

  constructor(
    private service: ProgramaService,
    private toast: ToastrService
  ) { }

  ngOnInit() {
    this.service.getProgramas().subscribe(programas => {
      this.rows = programas;
    });
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    const temp = this.temp.filter(function (d) {
      return d.productTitle.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.rows = temp;
  }

  onSelect({ selected }) {
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
    if (selected.length === 1) {
      this.isToolbarActive = true;
      this.itemCount = selected.length;
      this.itemsSelected = 'Item Selected';
    } else if (selected.length > 0) {
      this.isToolbarActive = true;
      this.itemCount = selected.length;
      this.itemsSelected = 'Items Selected';
    } else {
      this.isToolbarActive = false;
    }
  }

  triggerClose(event) {
    this.rows = this.temp;
    this.searchValue = '';
    this.isSearchActive = !this.isSearchActive;
  }

  onActivate(event) {

  }

  add() {
    this.rows.push({});
    this.editing[(this.rows.length - 1) + '-nome'] = true;
    this.rows = [...this.rows];
  }


  remove() {
    this.selected.map(obj => {
      this.service.removePrograma(obj.id).then(() => {
        this.toast.success('Programa excluído com sucesso', 'Sucesso');
      });
    });
    this.isToolbarActive = false;
    this.selected = [];
  }

  updateValue(event, cell, rowIndex) {
    console.log('inline editing rowIndex', rowIndex)
    this.editing[rowIndex + '-' + cell] = false;
    this.rows[rowIndex][cell] = event.target.value;
    const programa = this.rows[rowIndex];
    if (programa.id) {
      this.service.updatePrograma(programa).then(() => {
        this.toast.success('Programa atualizado com sucesso.', 'Sucesso');
      });
    } else {
      if (programa.nome) {
        this.service.addPrograma(programa).then(() => {
          this.toast.success('Programa incluído com sucesso.', 'Sucesso');
        });
      }
    }
    this.rows = [...this.rows];
    console.log('UPDATED!', this.rows[rowIndex][cell]);
  }

}
