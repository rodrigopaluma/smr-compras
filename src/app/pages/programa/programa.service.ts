import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

export interface Programa {
  id: string,
  nome: string;
}
@Injectable()
export class ProgramaService {

  constructor(private db: AngularFirestore) { }

  getProgramas(): Observable<Programa[]> {
    return this.db.collection<Programa>('programas').valueChanges();
  }

  getPrograma(id: string): Observable<Programa> {
    return this.db.collection('programas').doc<Programa>(id).valueChanges();
  }

  addPrograma(programa: Programa): Promise<boolean> {
    programa.id = this.db.createId();
    // tslint:disable-next-line:max-line-length
    return this.db.collection('programas').doc(programa.id).set(programa).then(() => true).catch((error) => { console.log(error); return false});
  }

  updatePrograma(programa: Programa): Promise<boolean> {
    // tslint:disable-next-line:max-line-length
    return this.db.collection('programas').doc(programa.id).update(programa).then(() => true).catch((error) => { console.log(error); return false});
  }

  removePrograma(id: string): Promise<boolean> {
    return this.db.collection('programas').doc(id).delete().then(() => true).catch((error) => { console.log(error); return false});
  }
}
