import {
	Component,
	OnInit,
	ElementRef,
	HostListener,
	HostBinding
} from '@angular/core';
import { GlobalState } from '../../../app.state';
import { ConfigService } from '../../../shared/services/config/config.service';
import { Router } from '@angular/router';
import { UsuarioService } from '../../usuario/usuario.service';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
	checked = false;
	toggleRegister = false;
	loginForm = this.formBuilder.group({
		login: this.formBuilder.control('', []),
		senha: this.formBuilder.control('', [])
	});

	constructor(private formBuilder: FormBuilder, private service: UsuarioService) {
	}

	ngOnInit() {
		this.service.logout();
	}

	login(email: string, pwd: string) {
		this.service.login(email, pwd);
	}

	reset(email) {
		this.service.resetPwd(email);
	}
}
