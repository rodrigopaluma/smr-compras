import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { LoginComponent } from "./login.component";
import { SharedModule } from "../../../shared/shared.module";
import { UsuarioService } from "../../usuario/usuario.service";

const LOGIN_ROUTE = [{ path: "", component: LoginComponent }];

@NgModule({
	declarations: [LoginComponent],
	imports: [CommonModule, SharedModule, RouterModule.forChild(LOGIN_ROUTE)],
	providers: [UsuarioService]
})
export class LoginModule {}
