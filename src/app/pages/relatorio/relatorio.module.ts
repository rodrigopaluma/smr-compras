import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { RelatorioComponent } from './relatorio.component';
import { MyDateRangePickerModule } from 'mydaterangepicker';
import { ChartsModule } from 'ng2-charts';

const RELATORIO_ROUTE = [
  { path: '', component: RelatorioComponent }
];

@NgModule({
  declarations: [RelatorioComponent],
  exports: [RelatorioComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(RELATORIO_ROUTE),
    NgxDatatableModule,
    MyDateRangePickerModule,
    ChartsModule
  ]

})
export class RelatorioModule { }
