import { Component, OnInit } from '@angular/core';
import { SolicitacaoService, Solicitacao } from '../solicitacao/solicitacao.service';
import { UsuarioService, Usuario } from '../usuario/usuario.service';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import { IMyDrpOptions } from '../../../../node_modules/mydaterangepicker';
import { DepartamentosService, Departamento } from '../departamentos/departamentos.service';
import { FornecedorService, Fornecedor } from '../fornecedores/fornecedores.service';

@Component({
  selector: 'app-relatorio',
  templateUrl: './relatorio.component.html',
  styleUrls: ['./relatorio.component.scss']
})
export class RelatorioComponent implements OnInit {
  rows = [];
  original = [];

  showFilter = true;

  usuario = this.usuarioService.getCurrentUser();

  showRecusadas = true;
  showFila = true;
  showCotacao = true;
  showNegociado = true;
  showPO = true;
  showRecebido = true;
  showExcluida = true;

  showNumero = true;
  showDataInclusao = true;
  showDataRecebimento = true;
  showTipo = true;
  showCategoria = true;
  showFornecedor = true;
  showValorInicial = true;
  showValorNegociado = true;
  showNumeroPO = true;
  showSolicitante = true;
  showDepartamento = true;
  showStatus = true;
  showComprador = true;

  fornecedores: Fornecedor[] = [];
  fornecedor: Fornecedor;
  departamentos: Departamento[] = [];
  departamento: Departamento;
  compradores: Usuario[] = [];
  comprador: Usuario;
  periodoAlteracao: any;
  periodoInclusao: any;
  valorInicial: number;
  valorNegociado: number;

  recusadas = 0;
  aceitas = 0;
  total = 0;


  myDateRangePickerOptions: IMyDrpOptions = {
    // other options...
    dateFormat: 'dd.mm.yyyy',
  };
  valorInicialRegra = 'maior-que';
  valorNegociadoRegra = 'menor-que';
  hideFilter;

  public pieChartLabels: string[] = ['Recusadas', 'Aceitas'];
  public pieChartData: number[] = [0, 0];
  public pieChartType = 'pie';

  options = {
    fieldSeparator: ',',
    quoteStrings: '"',
    decimalseparator: '.',
    showLabels: true,
    headers: [],
    showTitle: false,
    title: '',
    useBom: false,
    removeNewLines: true
  };

  constructor(
    private service: SolicitacaoService,
    private usuarioService: UsuarioService,
    private departamentoService: DepartamentosService,
    private fornecedorService: FornecedorService
  ) { }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    // filter our data
    const temp = this.original.filter(function (d) {
      return d.numero.indexOf(val) !== -1 || !val;
    });
    // update the rows
    this.rows = temp;
    // Whenever the filter changes, always go back to the first page
    //  this.table.offset = 0;
  }
  ngOnInit() {
    this.fornecedorService.getFornecedores().subscribe(fornecedores => {
      this.fornecedores = fornecedores;
      console.log(this.fornecedores);
    });
    this.departamentoService.getDepartamentos().subscribe(departamentos => {
      this.departamentos = departamentos;
    });
    this.usuarioService.getCompradores().subscribe(compradores => {
      this.compradores = compradores;
    });
    this.service.getSolicitacoes().subscribe(solicitacoes => {
      this.rows = solicitacoes;
      this.rows.map (solicitacao => {
        if (solicitacao.alt === 'Recusada') {
          this.recusadas = this.recusadas + 1;
        } else {
          this.aceitas = this.aceitas + 1;
        }
      });
      this.total = this.recusadas + this.aceitas;
      this.pieChartData = [this.recusadas, this.aceitas];
      this.pieChartData = [...this.pieChartData];
      this.original = solicitacoes;
    });
    this.filtrar();
  }

  makeReport() {
    const data = [];
    this.options.headers = [];
    if (this.showNumero) {
      this.options.headers.push('Número');
    };
    if (this.showDataInclusao) {
      this.options.headers.push('Data de Inclusão');
    };
    if (this.showDataRecebimento) {
      this.options.headers.push('Data de Alteração');
    };
    if (this.showTipo) {
      this.options.headers.push('Tipo de Material');
    };
    if (this.showCategoria) {
      this.options.headers.push('Categoria');
    };
    if (this.showFornecedor) {
      this.options.headers.push('Fornecedor');
    };
    if (this.showValorInicial) {
      this.options.headers.push('Valor Inicial');
    };
    if (this.showValorNegociado) {
      this.options.headers.push('Valor Negociado');
    };
    if (this.showNumeroPO) {
      this.options.headers.push('Número da PO');
    };
    if (this.showSolicitante) {
      this.options.headers.push('Solicitante');
    };
    if (this.showDepartamento) {
      this.options.headers.push('Departamento');
    };
    if (this.showStatus) {
      this.options.headers.push('Status');
    };
    if (this.showComprador) {
      this.options.headers.push('Comprador');
    };
    this.options.headers.push('Escopo');
    this.options.headers.push('ObjectID');
    this.options.headers.push('Urgente');
    this.options.headers.push('Prazo');
    this.rows.map(sol => {
      const obj = {};
      if (this.showNumero) {
        obj['numero'] = sol.numero
      };
      if (this.showDataInclusao) {
        obj['inclusao'] = sol.inclusao.toDate().toISOString().substr(0, 10).split('-').reverse().join('/')
      };
      if (this.showDataRecebimento) {
        obj['alteracao'] = sol.alteracao ? sol.alteracao.toDate().toISOString().substr(0, 10).split('-').reverse().join('/') : sol.inclusao.toDate().toISOString().substr(0, 10).split('-').reverse().join('/')
      };
      if (this.showTipo) {
        obj['tipo'] = sol.tipoDeMaterial
      };
      if (this.showCategoria) {
        obj['categoria'] = sol.material
      };
      if (this.showFornecedor) {
        obj['fornecedor'] = this.resolveFornecedor(sol.fornecedorEscolhido)
      };
      if (this.showValorInicial) {
        sol.valorInicial ? obj['inicial'] = sol.valorInicial : obj['inicial'] = null
      };
      if (this.showValorNegociado) {
        sol.valorNegociado ? obj['negociado'] = sol.valorNegociado : obj['negociado'] = null
      };
      if (this.showNumeroPO) {
        sol.numeroPO ? obj['numeroPO'] = sol.numeroPO : obj['numeroPO'] = null
      };
      if (this.showSolicitante) {
        obj['solicitante'] = sol.solicitante
      };
      if (this.showDepartamento) {
        obj['departamento'] = sol.departamento
      };
      if (this.showStatus) {
        obj['status'] = sol.alt
      };
      if (this.showComprador) {
        sol.comprador ? obj['comprador'] = sol.comprador : obj['comprador'] = null
      };
      obj['descricao'] = sol.escopo;
      obj['ObjectID'] = sol.id;
      sol.urgente === true ? obj['urgente'] = true : obj['urgente'] = false;
      sol.prazo ? obj['prazo'] = sol.prazo : obj['prazo'] = null;
      data.push(obj)
    });
    // tslint:disable-next-line:no-unused-expression
    new Angular2Csv(data, 'Compras', this.options);
  }

  filtrar(event?: any, target?: string) {
    this.rows = this.original.filter(sol => {
      if (sol.alt === 'Recusada' && this.showRecusadas) { return sol }
      if (sol.alt === 'Em Fila' && this.showFila) { return sol }
      if (sol.alt === 'Em Cotação' && this.showCotacao) { return sol }
      if (sol.alt === 'Negociado' && this.showNegociado) { return sol }
      if (sol.alt === 'PO Emitido' && this.showPO) { return sol }
      if (sol.alt === 'Recebido' && this.showRecebido) { return sol }
      if (sol.alt === 'Excluida' && this.showExcluida) { return sol }
    });
    if (target === 'inclusao' || this.periodoInclusao) {
      const inicio: Date = event.beginJsDate || this.periodoInclusao.beginJsDate;
      const fim: Date = event.endJsDate || this.periodoInclusao.endJsDate;
      inicio.setHours(0, 0, 1);
      fim.setHours(0, 0, 1);
      fim.setDate(fim.getDate() + 1);
      this.rows = this.rows.filter(sol => {
        console.log(target, this.periodoInclusao)
        if (sol.inclusao.toDate() >= inicio && sol.inclusao.toDate() < fim) {
          return sol
        }
      });
    }
    if (target === 'alteracao' || this.periodoAlteracao) {
      const inicio: Date = event.beginJsDate || this.periodoAlteracao.beginJsDate;
      const fim: Date = event.endJsDate || this.periodoAlteracao.endJsDate;
      inicio.setHours(0, 0, 1);
      fim.setHours(0, 0, 1);
      fim.setDate(fim.getDate() + 1);
      this.rows = this.rows.filter(sol => {
        console.log(sol.alteracao.toDate(), inicio, fim)
        if (sol.alteracao.toDate() >= inicio && sol.alteracao.toDate() < fim) {
          return sol
        }
      });
    }
    if (this.departamento) {
      this.rows = this.rows.filter(sol => {
        console.log(sol.departamento, this.departamento);
        if (this.departamento === sol.departamento) {
          return sol;
        }
      });
    }
    if (this.fornecedor) {
      this.rows = this.rows.filter(sol => {
        if (this.fornecedor === sol.fornecedorEscolhido) {
          return sol;
        }
      });
    }
    if (this.comprador) {
      this.rows = this.rows.filter(sol => {
        if (this.comprador === sol.comprador) {
          return sol;
        }
      });
    }
    if (this.valorInicial) {
      if (this.valorInicialRegra === 'maior que') {
        this.rows = this.rows.filter(sol => {
          if (this.valorInicial >= sol.valorInicial) {
            return sol;
          }
        });
      } else {
        this.rows = this.rows.filter(sol => {
          if (this.valorInicial <= sol.valorInicial) {
            return sol;
          }
        });
      }
    }

    if (this.valorNegociado) {
      if (this.valorNegociadoRegra === 'maior que') {
        this.rows = this.rows.filter(sol => {
          if (this.valorNegociado >= sol.valorNegociado) {
            return sol;
          }
        });
      } else {
        this.rows = this.rows.filter(sol => {
          if (this.valorNegociado <= sol.valorNegociado) {
            return sol;
          }
        });
      }
    }

    this.aceitas = this.recusadas = this.total = 0;

    this.rows.map (solicitacao => {
      if (solicitacao.alt === 'Recusada') {
        this.recusadas = this.recusadas + 1;
      } else {
        this.aceitas = this.aceitas + 1;
      }
    });
    this.total = this.recusadas + this.aceitas;
    this.pieChartData = [this.recusadas, this.aceitas];
    this.pieChartData = [...this.pieChartData];
  }

  resolveFornecedor(id: string): string {
    let index = -1;
    this.fornecedores.findIndex((fornecedor, _index, _obj): boolean => {
      if (fornecedor.id === id) {
        index = _index
        return true;
      } else {
        return false
      }
    });
    return index >= 0 ? this.fornecedores[index].nome : '';
  }

}
