import { Component, OnInit, OnChanges } from '@angular/core';
import { MoedasService } from './moedas.service';
import { ToastrService } from '../../../../node_modules/ngx-toastr';

@Component({
  selector: 'app-moedas',
  templateUrl: './moedas.component.html',
  styleUrls: ['./moedas.component.scss']
})
export class MoedasComponent implements OnInit {

  rows = [];
  selected = [];
  temp = [];
  searchValue: string = null;
  isSearchActive = false;
  isToolbarActive = false;
  itemsSelected = '';
  itemCount = 0;
  editing = {};


  constructor(
    private service: MoedasService,
    private toast: ToastrService
  ) { }

  ngOnInit() {
    this.service.getMoedas().subscribe(moedas => {
      this.rows = moedas;
    });
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    const temp = this.temp.filter(function (d) {
      return d.productTitle.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.rows = temp;
  }

  onSelect({ selected }) {
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
    if (selected.length === 1) {
      this.isToolbarActive = true;
      this.itemCount = selected.length;
      this.itemsSelected = 'Item Selected';
    } else if (selected.length > 0) {
      this.isToolbarActive = true;
      this.itemCount = selected.length;
      this.itemsSelected = 'Items Selected';
    } else {
      this.isToolbarActive = false;
    }
  }

  triggerClose(event) {
    this.rows = this.temp;
    this.searchValue = '';
    this.isSearchActive = !this.isSearchActive;
  }

  onActivate(event) {

  }

  add() {
    this.rows.push({});
    this.editing[(this.rows.length - 1) + '-moeda'] = true;
    this.editing[(this.rows.length - 1) + '-singular'] = true;
    this.editing[(this.rows.length - 1) + '-plural'] = true;
    this.editing[(this.rows.length - 1) + '-simbolo'] = true;
    this.rows = [...this.rows];
  }


  remove() {
    this.selected.map(obj => {
      this.service.removeMoeda(obj.id).then(() => {
        this.toast.success('Moeda excluída com sucesso', 'Sucesso');
      });
    });
    this.isToolbarActive = false;
    this.selected = [];
  }

  updateValue(event, cell, rowIndex) {
    console.log('inline editing rowIndex', rowIndex)
    this.editing[rowIndex + '-' + cell] = false;
    this.rows[rowIndex][cell] = event.target.value;
    const moeda = this.rows[rowIndex];
    if (moeda.id) {
      this.service.updateMoeda(moeda).then(() => {
        this.toast.success('Moeda atualizada com sucesso.', 'Sucesso');
      });
    } else {
      if (moeda.moeda && moeda.singular && moeda.plural && moeda.simbolo) {
        this.service.addMoeda(moeda).then(() => {
          this.toast.success('Moeda incluída com sucesso.', 'Sucesso');
        });
      }
    }
    this.rows = [...this.rows];
    console.log('UPDATED!', this.rows[rowIndex][cell]);
  }

}
