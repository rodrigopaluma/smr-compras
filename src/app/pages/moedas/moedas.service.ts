import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

export interface Moeda {
  id: string,
  singular: string;
  plural: string;
  simbolo: string;
  moeda: string;
}
@Injectable()
export class MoedasService {

  constructor(private db: AngularFirestore) { }

  getMoedas(): Observable<Moeda[]> {
    return this.db.collection<Moeda>('moedas').valueChanges();
  }

  getMoeda(id: string): Observable<Moeda> {
    return this.db.collection('moedas').doc<Moeda>(id).valueChanges();
  }

  addMoeda(moeda: Moeda): Promise<boolean> {
    moeda.id = this.db.createId();
    return this.db.collection('moedas').doc(moeda.id).set(moeda).then(() => true).catch((error) => { console.log(error); return false});
  }

  updateMoeda(moeda: Moeda): Promise<boolean> {
    return this.db.collection('moedas').doc(moeda.id).update(moeda).then(() => true).catch((error) => { console.log(error); return false});
  }

  removeMoeda(id: string): Promise<boolean> {
    return this.db.collection('moedas').doc(id).delete().then(() => true).catch((error) => { console.log(error); return false});
  }
}
