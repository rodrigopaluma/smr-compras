import { NgModule } from '@angular/core';
import { MoedasComponent } from './moedas.component';
import { SharedModule } from '../../shared/shared.module';
import { RouterModule } from '@angular/router';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

const ROUTE = [
  { path: 'list', component: MoedasComponent },
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(ROUTE),
    NgxDatatableModule
  ],
  declarations: [MoedasComponent],
  exports: [MoedasComponent]
})
export class MoedasModule { }
