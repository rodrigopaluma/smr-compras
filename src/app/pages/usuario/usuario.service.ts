import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs/Observable';
import { AngularFireAuth } from '@angular/fire/auth';
import { ToastrService } from '../../../../node_modules/ngx-toastr';
import { Router} from '../../../../node_modules/@angular/router';
import * as firebase from 'firebase';
import { BehaviorSubject } from 'rxjs/BehaviorSubject'

export interface Usuario {
  id: string;
  foto: string;
  nome: string;
  role: string;
  email: string;
  departamento: string;
  slug: string;
  token: string;
}

@Injectable()
export class UsuarioService {

  usuario: Usuario
  messaging = firebase.messaging()
  currentMessage = new BehaviorSubject(null)

  constructor(
    private db: AngularFirestore,
    public afAuth: AngularFireAuth,
    private toast: ToastrService,
    private router: Router
  ) { }

  getCurrentUser(): Usuario {
    return <Usuario>{ ...JSON.parse(sessionStorage.getItem('currentUser')) }
  }

  getUsuarios(): Observable<Usuario[]> {
    return this.db.collection<Usuario>('usuarios').valueChanges();
  }

  getUsuarioNome(slug: string): Observable<Usuario[]> {
    return this.db.collection<Usuario>('usuarios', query => query.where('slug', '==', slug)).valueChanges();
  }

  getUsuarioNome2(nome: string): Observable<Usuario[]> {
    return this.db.collection<Usuario>('usuarios', query => query.where('nome', '==', nome)).valueChanges();
  }

  getCompradores(): Observable<Usuario[]> {
    return this.db.collection<Usuario>('usuarios', query => query.where('role', '==', 'administrador')).valueChanges();
  }

  getUsuario(id: string): Observable<Usuario> {
    return this.db.collection('usuarios').doc<Usuario>(id).valueChanges();
  }

  addUsuario(usuario: Usuario, pwd: string): Promise<boolean> {
    return this.afAuth.auth.createUserWithEmailAndPassword(usuario.email, pwd).then(credential => {
      usuario.id = credential.user.uid;
      return this.db.collection('usuarios').doc(usuario.id).set(usuario).then(() => true).catch((error) => { console.log(error); return false });
    });
  }

  updateUsuario(usuario: Usuario): Promise<boolean> {
    // tslint:disable-next-line:max-line-length
    return this.db.collection('usuarios').doc(usuario.id).update(usuario).then(() => true).catch((error) => { console.log(error); return false });
  }

  removeUsuario(id: string): Promise<boolean> {
    return this.db.collection('usuarios').doc(id).delete().then(() => true).catch((error) => { console.log(error); return false });
  }

  changePwd(email: string, pwdOld: string, pwdNew: string) {
    const user = this.afAuth.auth.currentUser;
    if (user) {
      return user.updatePassword(pwdNew).then(() => true).catch(() => false);
    } else {
      this.afAuth.auth.signInWithEmailAndPassword(email, pwdOld).then(credential => {
        credential.user.updatePassword(pwdNew).then(() => true).catch(() => false);
      });
    }
  }

  login(email: string, pwd: string) {
    this.afAuth.auth.signInWithEmailAndPassword(email, pwd)
      .then((credential) => {
        const obs = this.getUsuario(credential.user.uid).subscribe(usuario => {
          this.usuario = usuario;
          sessionStorage.setItem('currentUser', JSON.stringify(usuario));
          this.router.navigate(['/solicitacao']);
        });
      }).catch((error) => {
        if (error.code === 'auth/wrong-password') {
          this.toast.error('Usuário não identificado.', 'Erro');
        } else {
          this.toast.error('Algo deu errado, não conseguimos efetuar seu login.', 'Erro');
        }
      });
  }

  logout() {
    this.afAuth.auth.signOut();
    sessionStorage.clear();
    this.router.navigate(['/authentication/login']);
  }

  resetPwd(email: string) {
    const self = this;
    console.log(email);
    this.afAuth.auth.sendPasswordResetEmail(email).then(function() {
      self.toast.success('Em alguns instantes o sistema enviará um e-mail com as instruções para a troca de senha. Essa operação pode levar alguns minutos.', 'Sucesso');
    }).catch(function(error) {
      console.log(error);
      self.toast.error('Algo deu errado, não conseguimos encontrar o usuário ou não foi possível enviar o e-mail. Tente novamente.', 'Erro');
    });
  }

}
