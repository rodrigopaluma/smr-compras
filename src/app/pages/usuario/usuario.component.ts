import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, CanLoad } from '@angular/router';
import { UsuarioService, Usuario } from './usuario.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Departamento, DepartamentosService } from '../departamentos/departamentos.service';
import { ToastrService } from '../../../../node_modules/ngx-toastr';
import { AngularFireStorage } from '@angular/fire/storage';
import { AcentosService } from '../../shared/services/acentos/acentos.service';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.scss']
})
export class UsuarioComponent implements OnInit {

  tipoPwdAtual = 'password';
  tipoPwdNovo = 'password';
  id = '';
  usuario: Usuario = <Usuario>{};
  usuarioForm: FormGroup;
  senhaForm: FormGroup;
  render = false;
  departamentos: Departamento[] = []
  me: Usuario  = <Usuario>{};

  constructor(
    private route: ActivatedRoute,
    private service: UsuarioService,
    private formBuilder: FormBuilder,
    private departamentoService: DepartamentosService,
    private toast: ToastrService,
    private router: Router,
    private acentos: AcentosService,
    private storage: AngularFireStorage
  ) { }

  ngOnInit() {
    this.me = this.service.getCurrentUser();
    this.departamentoService.getDepartamentos().subscribe(departamentos => this.departamentos = departamentos);
    this.route.params.subscribe(
      params => {
        this.senhaForm = this.formBuilder.group({
          senhaAtual: this.formBuilder.control('', [Validators.minLength(6)]),
          senhaNova: this.formBuilder.control('', [Validators.minLength(6)])
        });
        if (params.id) {
          this.id = params.id;
          this.service.getUsuarioNome(this.id).subscribe(usuario => {
            this.usuario = usuario[0];
            this.usuarioForm = this.formBuilder.group({
              nome: this.formBuilder.control(this.usuario.nome, [Validators.required]),
              email: this.formBuilder.control({value: this.usuario.email, disabled: this.me.role.includes('normal')}, [Validators.required]),
              departamento: this.formBuilder.control({value: this.usuario.departamento, disabled: this.me.role.includes('normal')}, [Validators.required]),
              role: this.formBuilder.control({value: this.usuario.role, disabled: this.me.role.includes('normal')}, [Validators.required])
            });
            // console.log(this.usuario);
            this.render = true;
          });
        } else {
          this.usuarioForm = this.formBuilder.group({
            nome: this.formBuilder.control('', [Validators.required]),
            email: this.formBuilder.control('', [Validators.required]),
            departamento: this.formBuilder.control('', [Validators.required]),
            role: this.formBuilder.control('', [Validators.required])
          });
          this.render = true;
        }
      }
    );
  }


  updateUser() {
    const userTmp: Usuario = <Usuario>{ ...this.usuario, ...this.usuarioForm.value };
    if (this.senhaForm.get('senhaAtual').touched && this.usuario.id) {
      this.service.changePwd(userTmp.email, this.senhaForm.get('senhaAtual').value, this.senhaForm.get('senhaNova').value).catch(() => this.toast.error('Sua senha não foi atualizada', 'Erro'));
    }
    if (userTmp.id) {
      this.service.updateUsuario(userTmp).then(() => {
        this.toast.success('Usuário atualizado com sucesso.', 'Sucesso');
        if (this.usuario.role !== 'normal') {
          this.router.navigate(['/usuario']);
        } else {
          this.router.navigate(['/solicitacao']);
        }
      });
    } else {
      userTmp.slug = this.acentos.remover(userTmp.nome);
      if (!userTmp.foto) {
        userTmp.foto = 'https://firebasestorage.googleapis.com/v0/b/smr-compras.appspot.com/o/Icon-user.png?alt=media&token=4e00f093-e7e1-4ffc-b322-dfae626e0b00';
      }
      this.service.addUsuario(userTmp, this.senhaForm.get('senhaAtual').value).then(() => { this.toast.success('Usuário incluído com sucesso.', 'Sucesso') }).catch(() => this.toast.error('Seu usuário não foi criado. A senha precisa ter no mínimo 6 caracteres', 'Erro'));
    }
  }

  handleFileInput(file: File) {
    const randomId = Math.random().toString(36).substring(2);
    const ref = this.storage.ref(randomId);
    const task = ref.put(file)
      .then(t => {
        t.ref.getDownloadURL().then(url => {
          this.usuario.foto = url;
        });
      })
      .catch(error => console.log(error));
  }

}
