import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UsuarioComponent } from './usuario.component';
import { SharedModule } from '../../shared/shared.module';
import { UsuarioListComponent } from './usuario-list.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { LoggedInGuard } from '../../shared/security/loggedin.guard';
import { AngularFireStorageModule } from '@angular/fire/storage';

const ROUTE = [
  { path: '', redirectTo: 'list', pathMatch: 'full'  },
  { path: 'list', component: UsuarioListComponent,  canActivate: [LoggedInGuard]},
  { path: 'novo', component: UsuarioComponent,  canActivate: [LoggedInGuard]},
  { path: ':id', component: UsuarioComponent,  canActivate: [LoggedInGuard] }
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(ROUTE),
    NgxDatatableModule,
    AngularFireStorageModule
  ],
  declarations: [UsuarioComponent, UsuarioListComponent]
})
export class UsuarioModule { }
