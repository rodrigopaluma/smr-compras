import { Component, OnInit } from '@angular/core';
import { UsuarioService } from './usuario.service';
import { DepartamentosService, Departamento } from '../departamentos/departamentos.service';

@Component({
  selector: 'app-usuario-list',
  templateUrl: './usuario-list.component.html',
  styleUrls: ['./usuario-list.component.scss']
})
export class UsuarioListComponent implements OnInit {

  rows = [];
  selected = [];
  temp = [];
  searchValue: string = null;
  isSearchActive = false;
  isToolbarActive = false;
  itemsSelected = '';
  itemCount = 0;
  editing = {};
  departamentos: Departamento[] = []
  usuariosOriginal = [];


  constructor(private service: UsuarioService, private departamentoService: DepartamentosService) { }

  ngOnInit() {
    this.departamentoService.getDepartamentos().subscribe(departamentos => this.departamentos = departamentos);
    this.service.getUsuarios().subscribe(usuarios => {
      this.rows = usuarios;
      this.usuariosOriginal = usuarios;
    });
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    console.log(val)
    this.rows = this.usuariosOriginal.filter(item => {
      if (item.nome.toLowerCase().includes(val)) {
        return item;
      }
    })
  }

  onSelect({ selected }) {
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
    if (selected.length === 1) {
      this.isToolbarActive = true;
      this.itemCount = selected.length;
      this.itemsSelected = 'Item Selected';
    } else if (selected.length > 0) {
      this.isToolbarActive = true;
      this.itemCount = selected.length;
      this.itemsSelected = 'Items Selected';
    } else {
      this.isToolbarActive = false;
    }
  }

  triggerClose(event) {
    this.rows = this.temp;
    this.searchValue = '';
    this.isSearchActive = !this.isSearchActive;
  }

  onActivate(event) {

  }

  remove() {
    this.selected = [];
  }

  updateValue(event, cell, rowIndex) {
    console.log('inline editing rowIndex', rowIndex)
    this.editing[rowIndex + '-' + cell] = false;
    this.rows[rowIndex][cell] = event.target.value;
    this.rows = [...this.rows];
    console.log('UPDATED!', this.rows[rowIndex][cell]);
  }

}
