import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SolicitacaoComponent } from './solicitacao.component';
import { SharedModule } from '../../shared/shared.module';
import { SolicitacoesListComponent } from './solicitacoes-list.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { FornecedoresModule } from '../fornecedores/fornecedores.module';
import { TipoMaterialModule } from '../tipo-material/tipo-material.module';
import { SolicitacoesArquivadasComponent } from './solicitacoes-arquivadas.component';
import { NgAisModule } from 'angular-instantsearch';

const SOLICITACAO_ROUTE = [
  { path: '', component: SolicitacoesListComponent },
  { path: 'arquivadas', component: SolicitacoesArquivadasComponent },
  { path: 'nova', component: SolicitacaoComponent },
  { path: ':id', component: SolicitacaoComponent },
];

@NgModule({
  declarations: [SolicitacaoComponent, SolicitacoesListComponent, SolicitacoesArquivadasComponent],
  exports: [SolicitacoesListComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(SOLICITACAO_ROUTE),
    NgxDatatableModule,
    FornecedoresModule,
    TipoMaterialModule,
    NgAisModule
  ]

})
export class SolicitacaoModule { }
