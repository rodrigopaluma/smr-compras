import { Component, OnInit } from '@angular/core';
import { SolicitacaoService } from './solicitacao.service';
import { ToastrService } from 'ngx-toastr';
import { UsuarioService } from '../usuario/usuario.service';
import { Router } from '@angular/router';
import { Angular2Csv } from 'angular2-csv';
import * as moment from 'moment';

@Component({
  selector: 'app-solicitacoes-arquivadas',
  templateUrl: './solicitacoes-arquivadas.component.html',
  styleUrls: ['./solicitacoes-arquivadas.component.scss']
})
export class SolicitacoesArquivadasComponent implements OnInit {

  rows = [];
  selected = [];
  temp = [];
  searchValue: string = null;
  isSearchActive = false;
  isToolbarActive = false;
  itemsSelected = '';
  itemCount = 0;
  usuario = this.usuarioService.getCurrentUser();
  showRecusadas = true;
  showFila = false;
  showCotacao = false;
  showNegociado = false;
  showPO = false;
  showRecebido = true;
  showExcluida = true;
  original = [];
  vm = this;

  options = {
    fieldSeparator: ',',
    quoteStrings: '"',
    decimalseparator: '.',
    showLabels: true,
    headers: ['Número', 'Categoria', 'Tipo', 'Solicitante', 'Departamento', 'Inclusão', 'Alteração', 'Status', 'Comprador'],
    showTitle: false,
    title: '',
    useBom: false,
    removeNewLines: true,
    keys: ['approved', 'age', 'name']
  };

  constructor(
    private service: SolicitacaoService,
    private toastr: ToastrService,
    private usuarioService: UsuarioService,
    private router: Router
  ) {

  }


  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    // filter our data
    const temp = this.original.filter(function (d) {
      return d.numero.indexOf(val) !== -1 || !val;
    });
    // update the rows
    this.rows = temp;
    // Whenever the filter changes, always go back to the first page
    //  this.table.offset = 0;
  }
  ngOnInit() {
    if (this.usuario.role === 'administrador') {
      this.service.getSolicitacoes().subscribe(solicitacoes => {
        this.rows = solicitacoes;
        this.original = solicitacoes;
        this.filtrar();
      });
    } else {
      this.service.getSolicitacoesDepartamento(this.usuario.departamento).subscribe(solicitacoes => {
        this.rows = solicitacoes;
        this.original = solicitacoes;
        this.filtrar();
      });
    }
  }

  onSelect({ selected }) {
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
    if (selected.length === 1) {
      this.isToolbarActive = true;
      this.itemCount = selected.length;
      this.itemsSelected = 'Item Selected';
    } else if (selected.length > 0) {
      this.isToolbarActive = true;
      this.itemCount = selected.length;
      this.itemsSelected = 'Items Selected';
    } else {
      this.isToolbarActive = false;
    }
  }
  triggerClose(event) {
    this.rows = this.original;
    this. showExcluida = this.showRecebido = this.showRecusadas = true;
    this.showCotacao = this.showFila = this.showNegociado = this.showPO = false;
    this.searchValue = '';
    this.isSearchActive = !this.isSearchActive;
    this.filtrar();
  }
  onActivate(event) {
    // console.log("Activate Event", event);
  }

  add() {
    this.selected.push(this.rows[1], this.rows[3]);
  }

  update() {
    this.selected = [this.rows[1], this.rows[3]];
  }

  remove() {
    this.selected = [];
  }

  makeReport() {
    // tslint:disable-next-line:no-unused-expression
    const data = [];
    this.rows.map(sol => {
      data.push({
        Número: sol.numero,
        Categoria: sol.material,
        Tipo: sol.tipoDeMaterial,
        Solicitante: sol.solicitante,
        Departamento: sol.departamento,
        Inclusão: sol.inclusao.toDate().toISOString().substr(0, 10).split('-').reverse().join('/'),
        Alteração: sol.alteracao.toDate().toISOString().substr(0, 10).split('-').reverse().join('/'),
        Status: sol.alt,
        Comprador: sol.comprador
      })
    });
    // tslint:disable-next-line:no-unused-expression
    new Angular2Csv(data, 'Compras', this.options);
  }

  filtrar() {
    this.rows = this.original.filter(sol => {
      if (sol.alt === 'Recusada' && this.showRecusadas) { return sol }
      if (sol.alt === 'Em Fila' && this.showFila) { return sol }
      if (sol.alt === 'Em Cotação' && this.showCotacao) { return sol }
      if (sol.alt === 'Negociado' && this.showNegociado) { return sol }
      if (sol.alt === 'PO Emitido' && this.showPO) { return sol }
      if (sol.alt === 'Recebido' && this.showRecebido) { return sol }
      if (sol.alt === 'Excluida' && this.showExcluida) { return sol }
    });
  }

  getRowClass(row) {
    const hoje = moment();
    let alvo = moment();
    row.tipoDeMaterial === 'RAS' ? alvo = addWeekdays(row.inclusao.toDate(), 15) : alvo = addWeekdays(row.inclusao.toDate(), 5);
    if (hoje > alvo && (row.alt == 'Em Fila' || row.alt == 'Em Cotação' || row.alt == 'Negociado')) {
      //console.log ('Previsão ', alvo, 'Atual', hoje);
      if(row.urgente){
        return 'foraPrazo urgente';
      } else {
        return 'foraPrazo'
      }
    }else {
      if(row.urgente){
        return 'urgente';
      }
    }
    function addWeekdays(date, days) {
      date = moment(date); // use a clone
      while (days > 0) {
        date = date.add(1, 'days');
        // decrease "days" only if it's a weekday.
        if (date.isoWeekday() !== 6 && date.isoWeekday() !== 7) {
          days -= 1;
        }
      }
      return date;
    }

  }



  delete() {
    this.selected.map(solicitacao => {
      solicitacao.alt = 'Excluida';
      solicitacao.img = 'lixeira.png'
      this.service.updateSolicitacao(solicitacao)
        .then(() => {
          this.toastr.success('Solicitação Excluída!', 'Sucesso').onHidden.subscribe(() => {
            this.router.navigate(['/solicitacao']);
          });
        }).catch(() => {
          this.toastr.error('Ocorreu um erro, tente novamente mais tarde.', 'Erro');
        });
    });
  }
}
