import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs/Observable';
import { UsuarioService } from '../usuario/usuario.service';
import { FornecedorService } from '../fornecedores/fornecedores.service';
import { MotivoRecusaService } from '../motivo-recusa/motivo-recusa.service';
import { timeout } from 'q';

export interface Solicitacao {
  id: string,
  alt: string;
  alteracao: Date;
  comprador: string;
  img: string;
  inclusao: Date;
  material: string;
  numero: string;
  solicitante: string;
  tipoDeMaterial: string;
  urgente?: boolean;

  reembolso?: boolean;
  descProjeto?: string;
  justificativa?: string;
  categoriaBudget?: string;
  cliente?: string;
  programa?: string;
  inicioProjeto?: Date;
  ppap?: Date;
  sop?: Date;

  exclusivo: boolean;
  escopo: string;
  orcamentos?: Orcamento[];
  produtos?: Produto[];
  documentos?: Documento[];
  aceite?: string;
  observacoes?: string;
  valorInicial?: number;
  valorNegociado?: number;
  fornecedorEscolhido?: string;
  cotacoes?: Cotacao[];
  numeroPO?: string;
  numeroGRS?: string;
  dataRecebimento?: Date;
  departamento?: string;

  motivo?: string;
  detalheMotivo?: string;

  historico?: Historico[];

  prazo?: any;
}

export interface Orcamento {
  id: string;
  fornecedor: string;
  valor: string;
  moeda: string;
}

export interface Produto {
  id: string;
  descricao: string;
  quantidade: string;
  unidadeMedida: string;
  dataDesejada: string;
}

export interface Documento {
  id: string;
  endereco: string;
  nome: string;
}

export interface Cotacao {
  id: string;
  endereco: string;
  nome: string;
}

export interface Historico {
  comprador: string;
  data: Date;
  emailComprador: string;
  emailSolicitante: string;
  id: string;
  imagem: string;
  msg: string;
  numeroSo: string;
  solicitante: string;
  status: string;
}

@Injectable()
export class SolicitacaoService {

  constructor(private db: AngularFirestore, private usuarioService: UsuarioService, private fornecedorService: FornecedorService, private motivoService: MotivoRecusaService) { }

  getSolicitacoes(): Observable<Solicitacao[]> {
    return this.db.collection<Solicitacao>('solicitacoes', query => query.orderBy('numero', 'desc')).valueChanges();
  }

  getMinhasSolicitacoes(usuario: string): Observable<Solicitacao[]> {
    return this.db.collection<Solicitacao>('solicitacoes', query => query.where('comprador', '==', usuario)).valueChanges();
  }

  getSolicitacoesUsuario(usuario: string): Observable<Solicitacao[]> {
    return this.db.collection<Solicitacao>('solicitacoes', query => query.where('solicitante', '==', usuario)).valueChanges();
  }

  getSolicitacoesDepartamento(dpto: string): Observable<Solicitacao[]> {
    return this.db.collection<Solicitacao>('solicitacoes', query => query.where('departamento', '==', dpto)).valueChanges();
  }

  getSolicitacao(id: string): Observable<Solicitacao> {
    return this.db.collection('solicitacoes').doc<Solicitacao>(id).valueChanges();
  }

  addSolicitacao(solicitacao: Solicitacao): Promise<string> {
    solicitacao.id = this.db.createId();
    // tslint:disable-next-line:max-line-length
    return this.db.collection('solicitacoes').doc(solicitacao.id).set(solicitacao)
      .then(() => {
        this.addHistorico(solicitacao);
        return solicitacao.id
      })
      .catch((error) => { console.log(error); return error });
  }

  addHistoricoParcial(solicitacao, hist, idHist) {
    if (hist.emailComprador && hist.emailSolicitante) {
      this.db.collection<Orcamento>(`solicitacoes/${solicitacao.id}/historico`).doc(idHist).set(hist);
    }
  }

  updateSolicitacao(solicitacao: Solicitacao, historico: boolean = true): Promise<boolean> {
    return this.db.collection('solicitacoes').doc(solicitacao.id)
      .update(solicitacao).then(() => {
        if (historico) {
          this.addHistorico(solicitacao);
        } else {
          const idHist = this.db.createId();
          const hist = {
            comprador: solicitacao.comprador || 'Não definido',
            data: new Date(),
            emailComprador: '',
            emailSolicitante: '',
            id: idHist,
            imagem: solicitacao.img,
            msg: `As informações da solicitação ${solicitacao.numero} foi alterada, porém não houve atualização do status`,
            numeroSo: solicitacao.numero,
            solicitante: solicitacao.solicitante,
            status: solicitacao.alt
          }
          this.usuarioService.getUsuarioNome2(solicitacao.solicitante).subscribe(solic => {
            if (solic && solic[0] && solic[0].id) {
              hist['emailSolicitante'] = solic[0].email;
              this.addHistoricoParcial(solicitacao, hist, idHist);
            }
          });
          this.usuarioService.getUsuarioNome2(solicitacao.comprador).subscribe(solic => {
            if (solic && solic[0] && solic[0].id) {
              hist['emailComprador'] = solic[0].email;
              this.addHistoricoParcial(solicitacao, hist, idHist);
            }
          });
        }
        return true;
      })
      .catch((error) => { console.log(error); return false });
  }

  removeSolicitacao(id: string): Promise<boolean> {
    return this.db.collection('solicitacoes').doc(id).delete().then(() => true).catch((error) => { console.log(error); return false });
  }

  getOrcamentos(sid: string): Observable<Orcamento[]> {
    return this.db.collection<Orcamento>(`solicitacoes/${sid}/orcamentos`).valueChanges();
  }

  addOrcamento(sid: string, orcamento: Orcamento) {
    orcamento.id = this.db.createId();
    return this.db.collection<Orcamento>(`solicitacoes/${sid}/orcamentos`).doc(orcamento.id).set(orcamento);
  }

  updateOrcamento(sid: string, orcamento: Orcamento) {
    return this.db.collection<Orcamento>(`solicitacoes/${sid}/orcamentos`).doc(orcamento.id).update(orcamento);
  }

  getProdutos(sid: string): Observable<Produto[]> {
    return this.db.collection<Produto>(`solicitacoes/${sid}/produtos`).valueChanges();
  }

  addProduto(sid: string, produto: Produto) {
    produto.id = this.db.createId();
    return this.db.collection<Orcamento>(`solicitacoes/${sid}/produtos`).doc(produto.id).set(produto);
  }

  updateProduto(sid: string, produto: Produto) {
    return this.db.collection<Orcamento>(`solicitacoes/${sid}/produtos`).doc(produto.id).update(produto);
  }

  getDocumentos(sid: string): Observable<Documento[]> {
    return this.db.collection<Documento>(`solicitacoes/${sid}/documentos`).valueChanges();
  }

  addDocumento(sid: string, documento: Documento) {
    documento.id = this.db.createId();
    return this.db.collection<Orcamento>(`solicitacoes/${sid}/documentos`).doc(documento.id).set(documento);
  }

  updateDocumento(sid: string, documento: Documento) {
    return this.db.collection<Orcamento>(`solicitacoes/${sid}/documentos`).doc(documento.id).update(documento);
  }

  getCotacoes(sid: string): Observable<Cotacao[]> {
    return this.db.collection<Cotacao>(`solicitacoes/${sid}/cotacoes`).valueChanges();
  }

  addCotacao(sid: string, cotacao: Cotacao) {
    cotacao.id = this.db.createId();
    return this.db.collection<Orcamento>(`solicitacoes/${sid}/cotacoes`).doc(cotacao.id).set(cotacao);
  }

  updateCotacao(sid: string, cotacao: Cotacao) {
    return this.db.collection<Orcamento>(`solicitacoes/${sid}/cotacoes`).doc(cotacao.id).update(cotacao);
  }

  addHistorico(sol: Solicitacao) {
    const sub = this.getSolicitacao(sol.id).subscribe(solicitacao => {
      let msg = '';
      if (solicitacao.alt === 'Em Fila' && !solicitacao.alteracao) {
        msg = `${solicitacao.solicitante} do departamento ${solicitacao.departamento} incluiu a solicitação ${solicitacao.numero} em ${new Date().toISOString().substring(0, 10)}`;
      } else if (solicitacao.alt === 'Em Fila' && solicitacao.alteracao) {
        msg = `${solicitacao.solicitante} do departamento ${solicitacao.departamento} reenviou a solicitação ${solicitacao.numero} em ${new Date().toISOString().substring(0, 10)}`;
      } else if (solicitacao.alt === 'Em Cotação') {
        msg = `A solicitação ${solicitacao.numero} foi aceita pelo comprador ${solicitacao.comprador} em ${new Date().toISOString().substring(0, 10)} e se encontra em fase de cotação`;
      } else if (solicitacao.alt === 'Recusada') {
        this.motivoService.getMotivoRecusa(solicitacao.motivo).subscribe(motivo => {
          msg = `A solicitação ${solicitacao.numero} foi recusada pelo comprador ${solicitacao.comprador} em ${new Date().toISOString().substring(0, 10)} conforme as seguintes observações: ${motivo.descricao}`;
          if (motivo.id === 'kAiMnJvYm4BBj3DoffOw') {
            msg = msg + ' - ' + solicitacao.detalheMotivo;
          }
          // tslint:disable-next-line:no-shadowed-variable
          const id: string = this.db.createId();
          // tslint:disable-next-line:no-shadowed-variable
          const historico = { id: id, solicitante: solicitacao.solicitante, data: new Date(), status: solicitacao.alt, imagem: solicitacao.img, msg: msg, descricao: solicitacao.escopo};
          if (solicitacao.numero) {
            historico['numeroSo'] = solicitacao.numero;
          }
          if (solicitacao.solicitante) {
            this.usuarioService.getUsuarioNome2(solicitacao.solicitante).subscribe(solic => {
              if (solic && solic[0] && solic[0].id) {
                historico['emailSolicitante'] = solic[0].email;
              }
              if (solicitacao && solicitacao.comprador && solicitacao.comprador.length > 0) {
                this.usuarioService.getUsuarioNome2(solicitacao.comprador).subscribe(compr => {
                  if (compr && compr[0] && compr[0].id) {
                    historico['emailComprador'] = compr[0].email;
                    historico['comprador'] = compr[0].nome;
                  }
                  this.db.collection<Orcamento>(`solicitacoes/${solicitacao.id}/historico`).doc(id).set(historico).then(() => { sub.unsubscribe() });
                });
              } else {
                this.db.collection<Orcamento>(`solicitacoes/${solicitacao.id}/historico`).doc(id).set(historico).then(() => { sub.unsubscribe() });
              }
            });
          }
        });
      } else if (solicitacao.alt === 'Negociado') {
        this.fornecedorService.getFornecedor(solicitacao.fornecedorEscolhido).subscribe(fornecedor => {
          msg = `A solicitação ${solicitacao.numero} foi negociada pelo comprador ${solicitacao.comprador} em ${new Date().toISOString().substring(0, 10)}. O fornecedor escolhido foi ${fornecedor.nome}`;
          const id: string = this.db.createId();
          const historico = { id: id, solicitante: solicitacao.solicitante, data: new Date(), status: solicitacao.alt, imagem: solicitacao.img, msg: msg, descricao: solicitacao.escopo };
          if (solicitacao.numero) {
            historico['numeroSo'] = solicitacao.numero;
          }
          if (solicitacao.solicitante) {
            this.usuarioService.getUsuarioNome2(solicitacao.solicitante).subscribe(solic => {
              if (solic && solic[0] && solic[0].id) {
                historico['emailSolicitante'] = solic[0].email;
              }
              if (solicitacao && solicitacao.comprador && solicitacao.comprador.length > 0) {
                this.usuarioService.getUsuarioNome2(solicitacao.comprador).subscribe(compr => {
                  if (compr && compr[0] && compr[0].id) {
                    historico['emailComprador'] = compr[0].email;
                    historico['comprador'] = compr[0].nome;
                  }
                  this.db.collection<Orcamento>(`solicitacoes/${solicitacao.id}/historico`).doc(id).set(historico).then(() => { sub.unsubscribe() });
                });
              } else {
                this.db.collection<Orcamento>(`solicitacoes/${solicitacao.id}/historico`).doc(id).set(historico).then(() => { sub.unsubscribe() });
              }
            });
          }
        });
      } else if (solicitacao.alt === 'PO Emitido') {
        msg = `A solicitação ${solicitacao.numero} teve a PO emitida em ${new Date().toISOString().substring(0, 10)}. Número da PO ${solicitacao.numeroPO}`;
      } else if (solicitacao.alt === 'Recebido') {
        const data: any = solicitacao.alteracao;
        msg = `A solicitação ${solicitacao.numero} foi recebida em ${data.toDate().toISOString().substring(0, 10)}.`;
      }
      if (solicitacao.alt !== 'Recusada' && solicitacao.alt !== 'Negociado') {
        const id: string = this.db.createId();
        const historico = { id: id, solicitante: solicitacao.solicitante, data: new Date(), status: solicitacao.alt, imagem: solicitacao.img, msg: msg, descricao: solicitacao.escopo };
        if (solicitacao.numero) {
          historico['numeroSo'] = solicitacao.numero;
        }
        if (solicitacao.solicitante) {
          this.usuarioService.getUsuarioNome2(solicitacao.solicitante).subscribe(solic => {
            if (solic && solic[0] && solic[0].id) {
              historico['emailSolicitante'] = solic[0].email;
            }
            if (solicitacao && solicitacao.comprador && solicitacao.comprador.length > 0) {
              this.usuarioService.getUsuarioNome2(solicitacao.comprador).subscribe(compr => {
                if (compr && compr[0] && compr[0].id) {
                  historico['emailComprador'] = compr[0].email;
                  historico['comprador'] = compr[0].nome;
                }
                this.db.collection<Orcamento>(`solicitacoes/${solicitacao.id}/historico`).doc(id).set(historico).then(() => { sub.unsubscribe() });
              });
            } else {
              this.db.collection<Orcamento>(`solicitacoes/${solicitacao.id}/historico`).doc(id).set(historico).then(() => { sub.unsubscribe() });
            }
          });
        }
      }
    });
  }

  getHistorico(sid: string){
    return this.db.collection<Historico>(`solicitacoes/${sid}/historico`, query => query.orderBy('data', 'asc')).valueChanges();
  }
}

