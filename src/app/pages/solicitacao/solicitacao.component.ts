import { Component, OnInit, ViewEncapsulation, ViewChild, ElementRef } from '@angular/core';
import { TipoMaterialService, TipoMaterial } from '../tipo-material/tipo-material.service';
import { MoedasService, Moeda } from '../moedas/moedas.service';
import { UnidadeMedidaService, UnidadeMedida } from '../unidade-medida/unidade-medida.service';
import { FornecedorService, Fornecedor } from '../fornecedores/fornecedores.service';
import { ActivatedRoute, Router } from '../../../../node_modules/@angular/router';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { SolicitacaoService, Solicitacao, Documento, Historico } from './solicitacao.service';
import { Observable, Subscription } from '../../../../node_modules/rxjs';
import { MotivoRecusa, MotivoRecusaService } from '../motivo-recusa/motivo-recusa.service';
import { ModalDirective } from '../../../../node_modules/ngx-bootstrap';
import { UsuarioService } from '../usuario/usuario.service';
import { ClienteService, Cliente } from '../cliente/cliente.service';
import { ProgramaService, Programa } from '../programa/programa.service';
import { CategoriaDoBudgetService, Categoria } from '../categoria-do-budget/categoria-do-budget.service';
import { ToastrService } from '../../../../node_modules/ngx-toastr';
import { ConfiguracaoService } from '../../shared/services/configuracao/configuracao.service';
import { AngularFireStorage } from '@angular/fire/storage';
import { ValueTransformer } from '../../../../node_modules/@angular/compiler/src/util';
import { startWith, map } from 'rxjs/operators';

@Component({
  selector: 'app-solicitacao',
  templateUrl: './solicitacao.component.html',
  styleUrls: ['./solicitacao.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SolicitacaoComponent implements OnInit {
  render = false;
  title = 'Nova Solicitação';
  tiposMateriais: TipoMaterial[] = [];
  tipoInternoMaterial;
  moedas: Moeda[] = [];
  unidadesMedida: UnidadeMedida[] = [];
  fornecedores: Fornecedor[] = [];
  orcamentos: FormGroup[] = [];
  produtos: FormGroup[] = [];
  documentos: FormGroup[] = [];
  cotacoes: FormGroup[] = [];
  programas: string[] = [];
  clientes: Cliente[] = [];
  categorias: Categoria[] = [];
  solicitacaoForm: FormGroup;
  observacoesForm: FormGroup;
  solicitacao: Solicitacao;
  motivos: MotivoRecusa[] = [];
  motivoString: string = '';
  editable = false;
  simboloMoeda = 'R$'
  usuario = this.usuarioService.getCurrentUser();
  moedaOption = { prefix: this.simboloMoeda, thousands: '.', decimal: ',' }
  @ViewChild('motivoModal') public modal: ModalDirective;
  showMotivo = false;
  tooltipRAS = `RAS: CAPEX significa investimento em Ativo Imobilizado. O ativo imobilizado de uma empresa
    são todos os bens adquiridos para uso nas próprias atividades de empresa, esperando que
    este seja utilizado por mais de um ano.\n
    São exemplos de ativos imobilizados: Terrenos, Edificios, Veículos, Móveis e utensílios, Máquinas, equipamentos e Ferramentas, Computadores, Máquinas em construção.
    O custo de um item do ativo imobilizado compreende:\n
    (a) seu preço de aquisição, acrescido de impostos de importação e impostos não recuperáveis sobre a compra;\n
    (b) quaisquer custos diretamente para colocar o ativo no local e em condição necessária para o mesmo ser capaz de funcionar da forma pretendida pela empresa;\n
    O valor para se considerar um item que tenha vida útil superior a 1 ano como bem do ativo
    imobilizado tem que ser superior a R$ 1.200,00.`
  tooltipGRS = `GRS: São considerados como custos ou despesas operacionais os gastos com reparos e
    conservação de bens e instalações destinados tão-somente a mantê-los em condições
    eficientes de operações, e que não resultem em aumento da vida útil do bem.\n
    Referem-se a serviços de manutenção preventiva ou corretiva com troca de peças tais como
    revisão sistemática e periódica do bem, na qual são feitas limpeza, lubrificação, substituição
    de peças desgastadas etc.\n
    Normalmente esse tipo de manutenção não está vinculado ao aumento de vida útil do bem,
    mas é necessário ao seu funcionamento normal, dentro de padrões técnicos de qualidade,
    normas de segurança etc.\n
    Conserto ou substituição de parte ou peças em razão de quebra ou avaria do equipamento,
    por imperícia ou outro problema técnico qualquer, necessários para que o bem retorne à
    sua condição normal de funcionamento, feito de forma isolada, o que normalmente não
    envolve acréscimo da vida útil da máquina ou equipamento.`
  itensComprasFilter = JSON.parse(localStorage.getItem('itensComprasFilter'));
  indexComprasFilter = null;
  previous = null;
  next = null;
  historico: Historico[] = [];
  filteredFornecedores

  constructor(
    private service: SolicitacaoService,
    private tipoMaterialService: TipoMaterialService,
    private moedaService: MoedasService,
    private unidadeMedidaService: UnidadeMedidaService,
    private fornecedorService: FornecedorService,
    private motivoService: MotivoRecusaService,
    private categoriaDoBudgetService: CategoriaDoBudgetService,
    private programaService: ProgramaService,
    private clienteService: ClienteService,
    private route: ActivatedRoute,
    private usuarioService: UsuarioService,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private configuracaoService: ConfiguracaoService,
    private storage: AngularFireStorage,
    private router: Router
  ) { }

  ngOnInit() {

    this.tipoMaterialService.getTiposMateriais().subscribe(tipos => {
      this.tiposMateriais = tipos;
    });
    this.moedaService.getMoedas().subscribe(moedas => {
      this.moedas = moedas;
    });
    this.unidadeMedidaService.getUnidadesMedida().subscribe(unidadesMedida => {
      this.unidadesMedida = unidadesMedida;
    });
    this.fornecedorService.getFornecedores().subscribe(fornecedores => {
      this.fornecedores = fornecedores;
    });
    this.motivoService.getMotivosRecusa().subscribe(motivos => {
      this.motivos = motivos
    });
    this.clienteService.getClientes().subscribe(clientes => {
      this.clientes = clientes;
    });
    this.programaService.getProgramas().subscribe(programas => {
      this.programas = programas.map(programa => programa.nome);
    });
    this.categoriaDoBudgetService.getCategorias().subscribe(categorias => {
      this.categorias = categorias;
    });
    this.route.params.subscribe(
      params => {
        if (params.id) {
          window.scroll(0,0);
          // Verifica se há um filtro gravado, caso exista ele habilita a navegação
          if (this.itensComprasFilter) {
            this.itensComprasFilter.map((item, index) => {
              if (item === params.id) {
                this.indexComprasFilter = index;
                this.indexComprasFilter > 0 ? this.previous = this.itensComprasFilter[this.indexComprasFilter - 1] : this.previous = null
                this.indexComprasFilter < this.itensComprasFilter.length - 1 ? this.next = this.itensComprasFilter[this.indexComprasFilter + 1] : this.next = null
              }
            })
          }
          // Edição de Solicitações
          this.observacoesForm = this.formBuilder.group({
            observacoes: this.formBuilder.control('', [])
          });
          this.service.getSolicitacao(params.id).subscribe(solicitacao => {
            // console.log(solicitacao);
            this.title = 'Solicitação ' + solicitacao.numero;
            this.solicitacao = solicitacao;
            if (this.solicitacao.alt === 'Em Fila' || this.solicitacao.alt === 'Recusada' || this.solicitacao.alt === 'Excluida') {
              if (this.solicitacao.solicitante === this.usuarioService.getCurrentUser().nome) {
                this.editable = false;
              } else {
                this.editable = true;
              }
              this.solicitacaoForm = this.formBuilder.group({
                urgente: this.formBuilder.control({ value: this.solicitacao.urgente, disabled: this.editable }, []),
                material: this.formBuilder.control({ value: this.solicitacao.material, disabled: this.editable }, [Validators.required]),
                tipoDeMaterial: this.formBuilder.control({ value: this.solicitacao.tipoDeMaterial, disabled: this.editable }, [Validators.required]),
                reembolso: this.formBuilder.control({ value: this.solicitacao.reembolso, disabled: this.editable }, []),
                descProjeto: this.formBuilder.control({ value: this.solicitacao.descProjeto, disabled: this.editable }, []),
                justificativa: this.formBuilder.control({ value: this.solicitacao.justificativa, disabled: this.editable }, [Validators.minLength(10)]),
                categoriaBudget: this.formBuilder.control({ value: this.solicitacao.categoriaBudget, disabled: this.editable }, []),
                cliente: this.formBuilder.control({ value: this.solicitacao.cliente, disabled: this.editable }, []),
                programa: this.formBuilder.control({ value: this.solicitacao.programa, disabled: this.editable }, []),
                inicioProjeto: this.formBuilder.control({ value: this.solicitacao.inicioProjeto, disabled: this.editable }, []),
                ppap: this.formBuilder.control({ value: this.solicitacao.ppap, disabled: this.editable }, []),
                sop: this.formBuilder.control({ value: this.solicitacao.sop, disabled: this.editable }, []),
                exclusivo: this.formBuilder.control({ value: this.solicitacao.exclusivo, disabled: this.editable }, [Validators.required]),
                escopo: this.formBuilder.control({ value: this.solicitacao.escopo, disabled: this.editable }, [Validators.required]),
                orcamentos: this.formBuilder.array([]),
                produtos: this.formBuilder.array([]),
                documentos: this.formBuilder.array([]),
                aceite: this.formBuilder.control({ value: this.solicitacao.aceite, disabled: this.editable }, []),
                observacoes: this.formBuilder.control({ value: this.solicitacao.observacoes, disabled: this.editable }, [Validators.required]),
                motivo: this.formBuilder.control('', [Validators.required]),
                detalheMotivo: this.formBuilder.control('', [Validators.required]),
                semDocumento: this.formBuilder.control({ value: this.solicitacao.documentos && this.solicitacao.documentos.length > 0 ? false : true, disabled: true }, []),
                prazo: this.formBuilder.control({value: this.solicitacao.prazo ? this.solicitacao.prazo.toDate().toISOString().substr(0, 10) : '' , disabled: this.usuarioService.getCurrentUser().role === 'Administrador'}, []),
              });
              this.getOrcamentos();
              this.getProdutos();
              this.getDocumentos();
              if (this.solicitacao.motivo) {
                this.motivoService.getMotivoRecusa(this.solicitacao.motivo).subscribe(motivo => this.motivoString = motivo.descricao)
              }

            } else if (this.solicitacao.alt === 'Em Cotação') {
              if (this.usuarioService.getCurrentUser().role === 'administrador') {
                this.editable = false;
              } else {
                this.editable = true;
              }
              this.solicitacaoForm = this.formBuilder.group({
                urgente: this.formBuilder.control({ value: this.solicitacao.urgente, disabled: true }, []),
                material: this.formBuilder.control({ value: this.solicitacao.material, disabled: true }, []),
                tipoDeMaterial: this.formBuilder.control({ value: this.solicitacao.tipoDeMaterial, disabled: true }, []),
                reembolso: this.formBuilder.control({ value: this.solicitacao.reembolso, disabled: true }, []),
                descProjeto: this.formBuilder.control({ value: this.solicitacao.descProjeto, disabled: true }, []),
                justificativa: this.formBuilder.control({ value: this.solicitacao.justificativa, disabled: true }, [Validators.minLength(10)]),
                categoriaBudget: this.formBuilder.control({ value: this.solicitacao.categoriaBudget, disabled: true }, []),
                cliente: this.formBuilder.control({ value: this.solicitacao.cliente, disabled: true }, []),
                programa: this.formBuilder.control({ value: this.solicitacao.programa, disabled: true }, []),
                inicioProjeto: this.formBuilder.control({ value: this.solicitacao.inicioProjeto, disabled: true }, []),
                ppap: this.formBuilder.control({ value: this.solicitacao.ppap, disabled: true }, []),
                sop: this.formBuilder.control({ value: this.solicitacao.sop, disabled: true }, []),
                exclusivo: this.formBuilder.control({ value: this.solicitacao.exclusivo, disabled: true }, []),
                escopo: this.formBuilder.control({ value: this.solicitacao.escopo, disabled: true }, []),
                orcamentos: this.formBuilder.array([]),
                produtos: this.formBuilder.array([]),
                documentos: this.formBuilder.array([]),
                aceite: this.formBuilder.control({ value: this.solicitacao.aceite, disabled: true }, []),
                observacoes: this.formBuilder.control({ value: this.solicitacao.observacoes, disabled: true }, []),
                semDocumento: this.formBuilder.control({ value: this.solicitacao.documentos && this.solicitacao.documentos.length > 0 ? false : true, disabled: true }, []),
                motivo: this.formBuilder.control('', []),
                valorInicial: this.formBuilder.control({ value: this.solicitacao.valorInicial === undefined ? '' : this.solicitacao.valorInicial, disabled: this.editable }, [Validators.required]),
                valorNegociado: this.formBuilder.control({ value: this.solicitacao.valorNegociado === undefined ? '' : this.solicitacao.valorNegociado, disabled: this.editable }, [Validators.required]),
                fornecedorEscolhido: this.formBuilder.control({ value: this.solicitacao.fornecedorEscolhido === undefined ? '' : this.solicitacao.fornecedorEscolhido, disabled: this.editable }, [Validators.required]),
                cotacoes: this.formBuilder.array([]),
                prazo: this.formBuilder.control({value: this.solicitacao.prazo ? this.solicitacao.prazo.toDate().toISOString().substr(0, 10) : '', disabled: this.usuarioService.getCurrentUser().role === 'Administrador'}, []),
              });
              this.getCotacoes();
              this.getOrcamentos();
              this.getProdutos();
              this.getDocumentos();
              this.filteredFornecedores = this.solicitacaoForm.get('fornecedorEscolhido').valueChanges.pipe(
                startWith(''),
                map(value => this._filter(value))
              );
            } else if (this.solicitacao.alt === 'Negociado') {
              if (this.usuarioService.getCurrentUser().role === 'administrador') {
                this.editable = false;
              } else {
                this.editable = true;
              }
              this.solicitacaoForm = this.formBuilder.group({
                urgente: this.formBuilder.control({ value: this.solicitacao.urgente, disabled: true }, []),
                material: this.formBuilder.control({ value: this.solicitacao.material, disabled: true }, [Validators.required]),
                tipoDeMaterial: this.formBuilder.control({ value: this.solicitacao.tipoDeMaterial, disabled: true }, [Validators.required]),
                reembolso: this.formBuilder.control({ value: this.solicitacao.reembolso, disabled: true }, []),
                descProjeto: this.formBuilder.control({ value: this.solicitacao.descProjeto, disabled: true }, []),
                justificativa: this.formBuilder.control({ value: this.solicitacao.justificativa, disabled: true }, [Validators.minLength(10)]),
                categoriaBudget: this.formBuilder.control({ value: this.solicitacao.categoriaBudget, disabled: true }, []),
                cliente: this.formBuilder.control({ value: this.solicitacao.cliente, disabled: true }, []),
                programa: this.formBuilder.control({ value: this.solicitacao.programa, disabled: true }, []),
                inicioProjeto: this.formBuilder.control({ value: this.solicitacao.inicioProjeto, disabled: true }, []),
                ppap: this.formBuilder.control({ value: this.solicitacao.ppap, disabled: true }, []),
                sop: this.formBuilder.control({ value: this.solicitacao.sop, disabled: true }, []),
                exclusivo: this.formBuilder.control({ value: this.solicitacao.exclusivo, disabled: true }, [Validators.required]),
                escopo: this.formBuilder.control({ value: this.solicitacao.escopo, disabled: true }, [Validators.required]),
                orcamentos: this.formBuilder.array([]),
                produtos: this.formBuilder.array([]),
                documentos: this.formBuilder.array([]),
                aceite: this.formBuilder.control({ value: this.solicitacao.aceite, disabled: true }, []),
                observacoes: this.formBuilder.control({ value: this.solicitacao.observacoes, disabled: true }, []),
                valorInicial: this.formBuilder.control({ value: this.solicitacao.valorInicial, disabled: true }, []),
                valorNegociado: this.formBuilder.control({ value: this.solicitacao.valorNegociado, disabled: true }, []),
                fornecedorEscolhido: this.formBuilder.control({ value: this.solicitacao.fornecedorEscolhido, disabled: true }, []),
                cotacoes: this.formBuilder.array([]),
                motivo: this.formBuilder.control('', []),
                numeroPO: this.formBuilder.control({ value: this.solicitacao.numeroPO, disabled: this.editable }, [Validators.required]),
                numeroGRS: this.formBuilder.control({ value: this.solicitacao.numeroGRS, disabled: !this.editable }, [Validators.required]),
                semDocumento: this.formBuilder.control({ value: this.solicitacao.documentos && this.solicitacao.documentos.length > 0 ? false : true, disabled: true }, [])
              });
              this.getCotacoes();
              this.getOrcamentos();
              this.getProdutos();
              this.getDocumentos();
            } else if (this.solicitacao.alt === 'PO Emitido') {
              if (this.usuarioService.getCurrentUser().role === 'administrador') {
                this.editable = false;
              } else {
                this.editable = true;
              }
              this.solicitacaoForm = this.formBuilder.group({
                urgente: this.formBuilder.control({ value: this.solicitacao.urgente, disabled: true }, []),
                material: this.formBuilder.control({ value: this.solicitacao.material, disabled: true }, [Validators.required]),
                tipoDeMaterial: this.formBuilder.control({ value: this.solicitacao.tipoDeMaterial, disabled: true }, [Validators.required]),
                reembolso: this.formBuilder.control({ value: this.solicitacao.reembolso, disabled: true }, []),
                descProjeto: this.formBuilder.control({ value: this.solicitacao.descProjeto, disabled: true }, []),
                justificativa: this.formBuilder.control({ value: this.solicitacao.justificativa, disabled: true }, [Validators.minLength(10)]),
                categoriaBudget: this.formBuilder.control({ value: this.solicitacao.categoriaBudget, disabled: true }, []),
                cliente: this.formBuilder.control({ value: this.solicitacao.cliente, disabled: true }, []),
                programa: this.formBuilder.control({ value: this.solicitacao.programa, disabled: true }, []),
                inicioProjeto: this.formBuilder.control({ value: this.solicitacao.inicioProjeto, disabled: true }, []),
                ppap: this.formBuilder.control({ value: this.solicitacao.ppap, disabled: true }, []),
                sop: this.formBuilder.control({ value: this.solicitacao.sop, disabled: true }, []),
                exclusivo: this.formBuilder.control({ value: this.solicitacao.exclusivo, disabled: true }, [Validators.required]),
                escopo: this.formBuilder.control({ value: this.solicitacao.escopo, disabled: true }, [Validators.required]),
                orcamentos: this.formBuilder.array([]),
                produtos: this.formBuilder.array([]),
                documentos: this.formBuilder.array([]),
                aceite: this.formBuilder.control({ value: this.solicitacao.aceite, disabled: true }, []),
                observacoes: this.formBuilder.control({ value: this.solicitacao.observacoes, disabled: true }, []),
                valorInicial: this.formBuilder.control({ value: this.solicitacao.valorInicial, disabled: true }, []),
                valorNegociado: this.formBuilder.control({ value: this.solicitacao.valorNegociado, disabled: true }, []),
                fornecedorEscolhido: this.formBuilder.control({ value: this.solicitacao.fornecedorEscolhido, disabled: true }, []),
                cotacoes: this.formBuilder.array([]),
                motivo: this.formBuilder.control('', []),
                semDocumento: this.formBuilder.control({ value: this.solicitacao.documentos && this.solicitacao.documentos.length > 0 ? false : true, disabled: true }, []),
                numeroPO: this.formBuilder.control({ value: this.solicitacao.numeroPO, disabled: true }, [Validators.required]),
                dataRecebimento: this.formBuilder.control({ value: this.usuarioService.getCurrentUser().role === 'administrador' ? new Date().toISOString().substr(0, 10) : '', disabled: this.editable }, [Validators.required]),
                numeroGRS: this.formBuilder.control({ value: this.solicitacao.numeroGRS, disabled: true }, [Validators.required])
              });
              this.getCotacoes();
              this.getOrcamentos();
              this.getProdutos();
              this.getDocumentos();
            } else if (this.solicitacao.alt === 'Recebido') {
              if (this.solicitacao.solicitante === this.usuarioService.getCurrentUser().nome) {
                this.editable = true;
              } else {
                this.editable = false;
              }
              this.solicitacaoForm = this.formBuilder.group({
                urgente: this.formBuilder.control({ value: this.solicitacao.urgente, disabled: true }, []),
                material: this.formBuilder.control({ value: this.solicitacao.material, disabled: this.editable }, [Validators.required]),
                tipoDeMaterial: this.formBuilder.control({ value: this.solicitacao.tipoDeMaterial, disabled: this.editable }, [Validators.required]),
                reembolso: this.formBuilder.control({ value: this.solicitacao.reembolso, disabled: true }, []),
                descProjeto: this.formBuilder.control({ value: this.solicitacao.descProjeto, disabled: true }, []),
                justificativa: this.formBuilder.control({ value: this.solicitacao.justificativa, disabled: true }, [Validators.minLength(10)]),
                categoriaBudget: this.formBuilder.control({ value: this.solicitacao.categoriaBudget, disabled: true }, []),
                cliente: this.formBuilder.control({ value: this.solicitacao.cliente, disabled: true }, []),
                programa: this.formBuilder.control({ value: this.solicitacao.programa, disabled: true }, []),
                inicioProjeto: this.formBuilder.control({ value: this.solicitacao.inicioProjeto, disabled: true }, []),
                ppap: this.formBuilder.control({ value: this.solicitacao.ppap, disabled: true }, []),
                sop: this.formBuilder.control({ value: this.solicitacao.sop, disabled: true }, []),
                exclusivo: this.formBuilder.control({ value: this.solicitacao.exclusivo, disabled: this.editable }, [Validators.required]),
                escopo: this.formBuilder.control({ value: this.solicitacao.escopo, disabled: this.editable }, [Validators.required]),
                orcamentos: this.formBuilder.array([]),
                produtos: this.formBuilder.array([]),
                documentos: this.formBuilder.array([]),
                motivo: this.formBuilder.control('', []),
                aceite: this.formBuilder.control({ value: this.solicitacao.aceite, disabled: this.editable }, []),
                observacoes: this.formBuilder.control({ value: this.solicitacao.observacoes, disabled: this.editable }, []),
                valorInicial: this.formBuilder.control({ value: this.solicitacao.valorInicial, disabled: this.editable }, []),
                valorNegociado: this.formBuilder.control({ value: this.solicitacao.valorNegociado, disabled: this.editable }, []),
                fornecedorEscolhido: this.formBuilder.control({ value: this.solicitacao.fornecedorEscolhido, disabled: this.editable }, []),
                cotacoes: this.formBuilder.array([]),
                numeroPO: this.formBuilder.control({ value: this.solicitacao.numeroPO, disabled: this.editable }, [Validators.required]),
                dataRecebimento: this.formBuilder.control({ value: this.solicitacao.dataRecebimento, disabled: this.editable }, [Validators.required]),
                numeroGRS: this.formBuilder.control({ value: this.solicitacao.numeroGRS, disabled: !this.editable }, [Validators.required]),
                semDocumento: this.formBuilder.control({ value: this.solicitacao.documentos && this.solicitacao.documentos.length > 0 ? false : true, disabled: true }, [])
              });
              this.getCotacoes();
              this.getOrcamentos();
              this.getProdutos();
              this.getDocumentos();
            }
            // Obtenção do histórico
            this.service.getHistorico(params.id).subscribe(hist => {
              this.historico = hist
            });
            if (this.solicitacaoForm.get('fornecedorEscolhido')) {
               this.filteredFornecedores = this.solicitacaoForm.get('fornecedorEscolhido').valueChanges
              .pipe(
                startWith(''),
                map(value => this._filter(value))
              );
            }
            this.render = true;
          });
        } else {
          // Solicitação Nova
          this.title = 'Nova Solicitação';
          this.solicitacaoForm = this.formBuilder.group({
            urgente: this.formBuilder.control('', []),
            material: this.formBuilder.control('', [Validators.required]),
            tipoDeMaterial: this.formBuilder.control('', [Validators.required]),
            reembolso: this.formBuilder.control('', []),
            descProjeto: this.formBuilder.control('', []),
            justificativa: this.formBuilder.control('', [Validators.minLength(10)]),
            categoriaBudget: this.formBuilder.control('', []),
            cliente: this.formBuilder.control('', []),
            programa: this.formBuilder.control('', []),
            inicioProjeto: this.formBuilder.control('', []),
            ppap: this.formBuilder.control('', []),
            sop: this.formBuilder.control('', []),
            exclusivo: this.formBuilder.control('', [Validators.required]),
            escopo: this.formBuilder.control('', [Validators.required]),
            orcamentos: this.formBuilder.array([this.createNewOrcamento()]),
            produtos: this.formBuilder.array([this.createNewProduto()]),
            documentos: this.formBuilder.array([this.createNewDocumento()]),
            motivo: this.formBuilder.control('', []),
            semDocumento: this.formBuilder.control({ value: true, disabled: true }, []),
            aceite: this.formBuilder.control('', []),
            prazo: this.formBuilder.control('', []),
          });
          // Inclui um observable para o autocomplete da primeira linha
          const fornecedor0 = this.orcamentosFormArray.at(0).get('fornecedor')
          this.filteredFornecedores = fornecedor0.valueChanges.pipe(
            startWith(''),
            map(value => this._filter(value))
          );
          this.render = true;
        }
      }
    );
  }

  // Filtro fornecedores
  private _filter(value): Fornecedor[] {
    if (typeof value === 'string') {
      const filterValue = value.toLowerCase();

      return this.fornecedores.filter(option => option.nome.toLowerCase().includes(filterValue));
    }

  }

  // Orçamentos

  createNewOrcamento(): FormGroup {
    return this.formBuilder.group({
      fornecedor: this.formBuilder.control('', [Validators.required]),
      valor: this.formBuilder.control(''),
      moeda: this.formBuilder.control('', [Validators.required]),
    })
  }

  addOrcamento() {
    this.orcamentosFormArray.push(this.formBuilder.group({
      fornecedor: this.formBuilder.control('', [Validators.required]),
      valor: this.formBuilder.control(''),
      moeda: this.formBuilder.control('', [Validators.required]),
    }))
    const fornecedorx = this.orcamentosFormArray.at(this.orcamentosFormArray.length - 1).get('fornecedor')
    this.filteredFornecedores = fornecedorx.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
  }

  removeOrcamento(index: number) {
    this.orcamentosFormArray.removeAt(index);
  }

  getOrcamentos() {
    this.service.getOrcamentos(this.solicitacao.id).subscribe(orcamentos => {
      if (orcamentos && orcamentos.length > 0) {
        orcamentos.map(orcamento => {
          this.moedas.map(moeda => {
            if (moeda.id === orcamento.moeda) {
              this.simboloMoeda = moeda.simbolo;
              this.moedaOption = { prefix: this.simboloMoeda, thousands: '.', decimal: ',' };
              this.moedaOption = { ...this.moedaOption };
            }
          })
        })
      }
      orcamentos.map(orcamento => {
        this.orcamentosFormArray.push(this.formBuilder.group({
          id: this.formBuilder.control({ value: orcamento.id, disabled: (this.solicitacao.alt === 'Em Fila' || this.solicitacao.alt === 'Recusada') ? this.editable : true }, []),
          fornecedor: this.formBuilder.control({ value: orcamento.fornecedor, disabled: (this.solicitacao.alt === 'Em Fila' || this.solicitacao.alt === 'Recusada') ? this.editable : true }, [Validators.required]),
          valor: this.formBuilder.control({ value: orcamento.valor, disabled: (this.solicitacao.alt === 'Em Fila' || this.solicitacao.alt === 'Recusada') ? this.editable : true }, [Validators.required]),
          moeda: this.formBuilder.control({ value: orcamento.moeda, disabled: (this.solicitacao.alt === 'Em Fila' || this.solicitacao.alt === 'Recusada') ? this.editable : true }, [Validators.required]),
        }))
      });
    });
  }

  get orcamentosFormArray(): FormArray {
    return this.solicitacaoForm.get('orcamentos') as FormArray;
  }

  // Produtos

  createNewProduto(): FormGroup {
    return this.formBuilder.group({
      descricao: this.formBuilder.control('', [Validators.required]),
      quantidade: this.formBuilder.control('', [Validators.required]),
      unidadeMedida: this.formBuilder.control('', [Validators.required]),
      dataDesejada: this.formBuilder.control('', []),
    })
  }

  addProduto() {
    this.produtosFormArray.push(this.formBuilder.group({
      descricao: this.formBuilder.control('', [Validators.required]),
      quantidade: this.formBuilder.control('', [Validators.required]),
      unidadeMedida: this.formBuilder.control('', [Validators.required]),
      dataDesejada: this.formBuilder.control('', []),
    }))
  }

  removeProduto(index: number) {
    this.produtosFormArray.removeAt(index);
  }

  getProdutos() {
    this.service.getProdutos(this.solicitacao.id).subscribe(produtos => {
      produtos.map(produto => {
        this.produtosFormArray.push(this.formBuilder.group({
          id: this.formBuilder.control({ value: produto.id, disabled: (this.solicitacao.alt === 'Em Fila' || this.solicitacao.alt === 'Recusada') ? this.editable : true }, []),
          descricao: this.formBuilder.control({ value: produto.descricao, disabled: (this.solicitacao.alt === 'Em Fila' || this.solicitacao.alt === 'Recusada') ? this.editable : true }, [Validators.required]),
          quantidade: this.formBuilder.control({ value: produto.quantidade, disabled: (this.solicitacao.alt === 'Em Fila' || this.solicitacao.alt === 'Recusada') ? this.editable : true }, [Validators.required]),
          dataDesejada: this.formBuilder.control({ value: produto.dataDesejada, disabled: (this.solicitacao.alt === 'Em Fila' || this.solicitacao.alt === 'Recusada') ? this.editable : true }, []),
          unidadeMedida: this.formBuilder.control({ value: produto.unidadeMedida, disabled: (this.solicitacao.alt === 'Em Fila' || this.solicitacao.alt === 'Recusada') ? this.editable : true }, [Validators.required]),
        }))
      });
    });
  }

  get produtosFormArray(): FormArray {
    return this.solicitacaoForm.get('produtos') as FormArray;
  }

  // Documentos

  createNewDocumento(): FormGroup {
    return this.formBuilder.group({
      endereco: this.formBuilder.control('', []),
      nome: this.formBuilder.control('', [])
    })
  }

  addDocumento() {
    this.documentosFormArray.push(this.formBuilder.group({
      endereco: this.formBuilder.control('', []),
      nome: this.formBuilder.control('', [])
    }))
  }

  removeDocumento(index: number) {
    this.documentosFormArray.removeAt(index);
  }

  getDocumentos() {
    this.service.getDocumentos(this.solicitacao.id).subscribe(documentos => {
      documentos.map(documento => {
        this.documentosFormArray.push(this.formBuilder.group({
          id: this.formBuilder.control({ value: documento.id, disabled: (this.solicitacao.alt === 'Em Fila' || this.solicitacao.alt === 'Recusada') ? this.editable : true }, []),
          endereco: this.formBuilder.control({ value: documento.endereco, disabled: (this.solicitacao.alt === 'Em Fila' || this.solicitacao.alt === 'Recusada') ? this.editable : true }, []),
          nome: this.formBuilder.control({ value: documento.nome, disabled: (this.solicitacao.alt === 'Em Fila' || this.solicitacao.alt === 'Recusada') ? this.editable : true }, [])
        }))
      });
    });
  }

  get documentosFormArray(): FormArray {
    return this.solicitacaoForm.get('documentos') as FormArray;
  }

  // Cotacoes

  createNewCotacao(): FormGroup {
    return this.formBuilder.group({
      endereco: this.formBuilder.control('', [Validators.required]),
      nome: this.formBuilder.control('', [Validators.required])
    })
  }

  addCotacao() {
    this.cotacoesFormArray.push(this.formBuilder.group({
      endereco: this.formBuilder.control('', [Validators.required]),
      nome: this.formBuilder.control('', [Validators.required])
    }))
  }

  removeCotacao(index: number) {
    this.cotacoesFormArray.removeAt(index);
  }

  getCotacoes() {
    this.service.getCotacoes(this.solicitacao.id).subscribe(documentos => {
      documentos.map(documento => {
        this.cotacoesFormArray.push(this.formBuilder.group({
          id: this.formBuilder.control({ value: documento.id, disabled: (this.solicitacao.alt === 'Em Cotação') ? this.editable : true }, [Validators.required]),
          endereco: this.formBuilder.control({ value: documento.endereco, disabled: (this.solicitacao.alt === 'Em Cotação') ? this.editable : true }, [Validators.required]),
          nome: this.formBuilder.control({ value: documento.nome, disabled: (this.solicitacao.alt === 'Em Cotação') ? this.editable : true }, [Validators.required])
        }))
      });
      if (documentos.length < 1) {
        const falta = 1 - documentos.length;
        for (let i = 0; i < falta; i++) {
          this.cotacoesFormArray.push(this.formBuilder.group({
            id: this.formBuilder.control({ value: '', disabled: (this.solicitacao.alt === 'Em Cotação') ? this.editable : true }, []),
            endereco: this.formBuilder.control({ value: '', disabled: (this.solicitacao.alt === 'Em Cotação') ? this.editable : true }, [Validators.required]),
            nome: this.formBuilder.control({ value: '', disabled: (this.solicitacao.alt === 'Em Cotação') ? this.editable : true }, [Validators.required])
          }))
        }
      }
    });
  }

  get cotacoesFormArray(): FormArray {
    return this.solicitacaoForm.get('cotacoes') as FormArray;
  }

  // Ajuste do Modal

  showMotivoModal() {
    this.modal.show();
  }


  // Salvamento
  save() {
    this.solicitacaoForm.get('observacoes').setValue(this.solicitacaoForm.get('observacoes') + '\n Solicitação retificada pelo usuário em ' + new Date().toString())
    if (this.solicitacaoForm.status !== 'VALID') {
      this.toastr.warning('Esse formulário não foi completamente preenchido ou contém erros e não pode ser salvo.', 'Aviso');
      const sol = Object.getOwnPropertyNames(this.solicitacaoForm.controls);
      sol.map(controle => {
        const obj = this.solicitacaoForm.get(controle);
        obj.markAsTouched()
      });
      this.orcamentosFormArray.controls.map(prod => {
        console.log(prod);
        const item: FormGroup = <FormGroup>prod;
        const itemNames = Object.getOwnPropertyNames(item.controls);
        itemNames.map(name => item.get(name).markAsTouched());
      });
      this.produtosFormArray.controls.map(prod => {
        console.log(prod);
        const item: FormGroup = <FormGroup>prod;
        const itemNames = Object.getOwnPropertyNames(item.controls);
        itemNames.map(name => item.get(name).markAsTouched());
      });
      this.documentosFormArray.controls.map(prod => {
        console.log(prod);
        const item: FormGroup = <FormGroup>prod;
        const itemNames = Object.getOwnPropertyNames(item.controls);
        itemNames.map(name => item.get(name).markAsTouched());
      });
      return;
    }
    const solicitacao: Solicitacao = { ...this.solicitacaoForm.value };
    solicitacao.id = this.solicitacao.id;
    const orcamentos = solicitacao.orcamentos;
    const produtos = solicitacao.produtos;
    const documentos = solicitacao.documentos;
    solicitacao.img = 'em-fila.png';
    solicitacao.alt = 'Em Fila';
    delete solicitacao.orcamentos;
    delete solicitacao.produtos;
    delete solicitacao.documentos;
    this.service.updateSolicitacao(solicitacao).then(sid => {
      orcamentos.map(orcamento => {
        if (orcamento.id) {
          this.service.updateOrcamento(solicitacao.id, orcamento)
        } else {
          this.service.addOrcamento(solicitacao.id, orcamento)
        }
      });
      produtos.map(produto => {
        if (produto.id) {
          this.service.updateProduto(solicitacao.id, produto)
        } else {
          this.service.addProduto(solicitacao.id, produto)
        }
      });
      documentos.map(documento => {
        if (documento.id) {
          this.service.updateDocumento(solicitacao.id, documento)
        } else {
          this.service.addDocumento(solicitacao.id, documento)
        }
      });
      this.toastr.success('Solicitação alterada com sucesso', 'Alterar Solicitação').onHidden.subscribe(() => {
        this.router.navigate(['/solicitacao']);
      });
    });
  }


  saveNewSolicitacao() {
    console.log(this.solicitacaoForm)
    if (this.solicitacaoForm.status !== 'VALID') {
      this.toastr.warning('Esse formulário não foi completamente preenchido ou contém erros e não pode ser salvo.', 'Aviso');
      const sol = Object.getOwnPropertyNames(this.solicitacaoForm.controls);
      sol.map(controle => {
        const obj = this.solicitacaoForm.get(controle);
        obj.markAsTouched()
      });
      this.orcamentosFormArray.controls.map(prod => {
        console.log(prod);
        const item: FormGroup = <FormGroup>prod;
        const itemNames = Object.getOwnPropertyNames(item.controls);
        itemNames.map(name => item.get(name).markAsTouched());
      });
      this.produtosFormArray.controls.map(prod => {
        console.log(prod);
        const item: FormGroup = <FormGroup>prod;
        const itemNames = Object.getOwnPropertyNames(item.controls);
        itemNames.map(name => item.get(name).markAsTouched());
      });
      this.documentosFormArray.controls.map(prod => {
        console.log(prod);
        const item: FormGroup = <FormGroup>prod;
        const itemNames = Object.getOwnPropertyNames(item.controls);
        itemNames.map(name => item.get(name).markAsTouched());
      });
      return;
    }
    const solicitacao: Solicitacao = { ...this.solicitacaoForm.value };
    const orcamentos = solicitacao.orcamentos;
    const produtos = solicitacao.produtos;
    const documentos = solicitacao.documentos;
    solicitacao.inclusao = new Date();
    solicitacao.solicitante = this.usuarioService.getCurrentUser().nome;
    solicitacao.img = 'em-fila.png';
    solicitacao.alt = 'Em Fila';
    solicitacao.departamento = this.usuarioService.getCurrentUser().departamento;
    delete solicitacao.orcamentos;
    delete solicitacao.produtos;
    delete solicitacao.documentos;
    const conf = this.configuracaoService.getConfiguracoes().subscribe(config => {
      solicitacao.numero = config.index + '-' + (config.ano - 2000);
      for (let i = solicitacao.numero.length; i < 7; i++) {
        solicitacao.numero = '0' + solicitacao.numero;
      }
      this.service.addSolicitacao(solicitacao).then(sid => {
        conf.unsubscribe();
        this.configuracaoService.updateConfiguracoes(config.index);
        orcamentos.map(orcamento => this.service.addOrcamento(sid, orcamento));
        produtos.map(produto => this.service.addProduto(sid, produto));
        documentos.map(documento => this.service.addDocumento(sid, documento));
        this.toastr.success('Solicitação incluída com sucesso', 'Nova Solicitação').onHidden.subscribe(() => {
          this.router.navigate(['/solicitacao']);
        });
      }
      );
    });
  }

  // Aprovação
  saveAprovacao() {
    this.solicitacaoForm.get('observacoes').disable();
    let solicitacao: Solicitacao = this.solicitacao;
    solicitacao = { ...this.solicitacaoForm.value };
    solicitacao.id = this.solicitacao.id;
    solicitacao.img = 'cotacao.png';
    solicitacao.alt = 'Em Cotação';
    solicitacao.comprador = this.usuarioService.getCurrentUser().nome;
    solicitacao.alteracao = new Date();
    delete solicitacao.orcamentos;
    delete solicitacao.produtos;
    delete solicitacao.documentos;
    !solicitacao.prazo ? delete solicitacao.prazo : solicitacao.prazo = new Date(solicitacao.prazo);
    this.service.updateSolicitacao(solicitacao)
      .then(() => {
        this.toastr.success('Solicitação Aprovada!', 'Sucesso').onHidden.subscribe(() => {
          this.router.navigate(['/solicitacao']);
        });
      }).catch(() => {
        this.toastr.error('Ocorreu um erro, tente novamente mais tarde.', 'Erro');
      });
  }

  // Save Comprador
  saveComprador() {
    const solicitacao: Solicitacao = <Solicitacao>{};
    solicitacao.id = this.solicitacao.id;
    solicitacao.comprador = this.usuarioService.getCurrentUser().nome;
    solicitacao.alteracao = new Date();
    this.service.updateSolicitacao(solicitacao, false)
      .then(() => {
        this.toastr.success('Solicitação Reinvidicada!', 'Sucesso').onHidden.subscribe(() => {
          this.router.navigate(['/solicitacao']);
        });
      }).catch(() => {
        this.toastr.error('Ocorreu um erro, tente novamente mais tarde.', 'Erro');
      });
  }

  // Reprovação
  saveReprovacao() {
    console.log(this.solicitacaoForm)
    if (this.solicitacaoForm.invalid) {
      this.solicitacaoForm.get('detalheMotivo').markAsTouched();
      this.toastr.warning('Esse formulário não foi completamente preenchido ou contém erros e não pode ser salvo.', 'Aviso');
      return;
    }
    let solicitacao: Solicitacao = this.solicitacao;
    solicitacao = { ...this.solicitacaoForm.value };
    solicitacao.id = this.solicitacao.id;
    solicitacao.img = 'recusada.png';
    solicitacao.alt = 'Recusada';
    solicitacao.alteracao = new Date();
    solicitacao.comprador = this.usuarioService.getCurrentUser().nome;
    delete solicitacao.orcamentos;
    delete solicitacao.produtos;
    delete solicitacao.documentos;
    this.service.updateSolicitacao(solicitacao)
      .then(() => {
        this.toastr.success('Solicitação Recusada!', 'Sucesso').onHidden.subscribe(() => {
          this.router.navigate(['/solicitacao']);
        });
      }).catch(() => {
        this.toastr.error('Ocorreu um erro, tente novamente mais tarde.', 'Erro');
      });
  }

  reenviar() {
    let solicitacao: Solicitacao = this.solicitacao;
    solicitacao = { ...this.solicitacaoForm.value };
    solicitacao.id = this.solicitacao.id;
    solicitacao.img = 'em-fila.png';
    solicitacao.alt = 'Em Fila';
    solicitacao.alteracao = new Date();
    solicitacao.comprador = '';
    delete solicitacao.orcamentos;
    delete solicitacao.produtos;
    delete solicitacao.documentos;
    this.service.updateSolicitacao(solicitacao)
      .then(() => {
        this.toastr.success('Solicitação Reenviada!', 'Sucesso').onHidden.subscribe(() => {
          this.router.navigate(['/solicitacao']);
        });
      }).catch(() => {
        this.toastr.error('Ocorreu um erro, tente novamente mais tarde.', 'Erro');
      });
  }

  // Parcial de orçamentos
  savePartial() {
    const solicitacao: Solicitacao = { ...this.solicitacaoForm.value };
    solicitacao.alteracao = new Date();
    solicitacao.id = this.solicitacao.id;
    if (!solicitacao.prazo) {
      delete solicitacao.prazo
    } else {
      solicitacao.prazo = new Date(solicitacao.prazo);
    }
    const cotacoes = solicitacao.cotacoes;
    delete solicitacao.cotacoes;
    delete solicitacao.orcamentos;
    delete solicitacao.produtos;
    delete solicitacao.documentos;

    this.service.updateSolicitacao(solicitacao, false)
      .then(() => {
        if (cotacoes) {
          cotacoes.map(cotacao => {
            if (cotacao.endereco) {
              this.service.addCotacao(solicitacao.id, cotacao);
            }
          });
        }
        this.toastr.success('Solicitação Atualizada!', 'Sucesso').onHidden.subscribe(() => {
          this.router.navigate(['/solicitacao']);
        });
      }).catch(() => {
        this.toastr.error('Ocorreu um erro, tente novamente mais tarde.', 'Erro');
      });
  }

  // Negociado
  saveNegociado() {
    if (this.solicitacaoForm.status !== 'VALID') {
      this.toastr.warning('Esse formulário não foi completamente preenchido ou contém erros e não pode ser salvo.', 'Aviso');
      console.log(this.solicitacaoForm)
      const sol = Object.getOwnPropertyNames(this.solicitacaoForm.controls);
      sol.map(controle => {
        const obj = this.solicitacaoForm.get(controle);
        obj.markAsTouched()
      });
      this.cotacoesFormArray.controls.map(prod => {
        const item: FormGroup = <FormGroup>prod;
        const itemNames = Object.getOwnPropertyNames(item.controls);
        itemNames.map(name => item.get(name).markAsTouched());
      });
      return;
    }
    const solicitacao: Solicitacao = { ...this.solicitacaoForm.value };
    const cotacoes = solicitacao.cotacoes;
    solicitacao.alteracao = new Date();
    solicitacao.id = this.solicitacao.id;
    solicitacao.img = 'negociado.png';
    solicitacao.alt = 'Negociado';
    if (!solicitacao.prazo) {
      delete solicitacao.prazo
    }  else {
      solicitacao.prazo = new Date(solicitacao.prazo);
    }
    delete solicitacao.orcamentos;
    delete solicitacao.produtos;
    delete solicitacao.documentos;
    delete solicitacao.cotacoes;
    this.service.updateSolicitacao(solicitacao)
      .then(() => {
        cotacoes.map(cotacao => {
          if (cotacao) {
            if (cotacao.id) {
              this.service.updateCotacao(this.solicitacao.id, cotacao);
            } else {
              this.service.addCotacao(this.solicitacao.id, cotacao);
            }
          }
        });
        this.toastr.success('Solicitação Atualizada!', 'Sucesso').onHidden.subscribe(() => {
          this.router.navigate(['/solicitacao']);
        });
      }).catch(() => {
        this.toastr.error('Ocorreu um erro, tente novamente mais tarde.', 'Erro');
      });
  }

  // Negociado
  savePOEmitida() {
    if (this.solicitacaoForm.status !== 'VALID') {
      this.toastr.warning('Esse formulário não foi completamente preenchido ou contém erros e não pode ser salvo.', 'Aviso');
      const sol = Object.getOwnPropertyNames(this.solicitacaoForm.controls);
      sol.map(controle => {
        const obj = this.solicitacaoForm.get(controle);
        obj.markAsTouched()
      });
      return;
    }
    const solicitacao: Solicitacao = { ...this.solicitacaoForm.value };
    solicitacao.alteracao = new Date();
    solicitacao.id = this.solicitacao.id;
    solicitacao.img = 'po-emitido.png';
    solicitacao.alt = 'PO Emitido';
    delete solicitacao.orcamentos;
    delete solicitacao.produtos;
    delete solicitacao.documentos;
    delete solicitacao.cotacoes;
    this.service.updateSolicitacao(solicitacao)
      .then(() => {
        this.toastr.success('Solicitação Atualizada!', 'Sucesso').onHidden.subscribe(() => {
          this.router.navigate(['/solicitacao']);
        });
      }).catch(() => {
        this.toastr.error('Ocorreu um erro, tente novamente mais tarde.', 'Erro');
      });
  }

  // Recebido
  saveRecebido() {
    if (this.solicitacaoForm.status !== 'VALID') {
      this.toastr.warning('Esse formulário não foi completamente preenchido ou contém erros e não pode ser salvo.', 'Aviso');
      const sol = Object.getOwnPropertyNames(this.solicitacaoForm.controls);
      sol.map(controle => {
        const obj = this.solicitacaoForm.get(controle);
        obj.markAsTouched()
      });
      return;
    }
    const solicitacao: Solicitacao = { ...this.solicitacaoForm.value };
    solicitacao.alteracao = new Date();
    solicitacao.id = this.solicitacao.id;
    solicitacao.img = 'recebido.png';
    solicitacao.alt = 'Recebido';
    delete solicitacao.orcamentos;
    delete solicitacao.produtos;
    delete solicitacao.documentos;
    this.service.updateSolicitacao(solicitacao)
      .then(() => {
        this.toastr.success('Solicitação Atualizada!', 'Sucesso').onHidden.subscribe(() => {
          this.router.navigate(['/solicitacao']);
        });
      }).catch(() => {
        this.toastr.error('Ocorreu um erro, tente novamente mais tarde.', 'Erro');
      });
  }

  // Manipulação dos Arquivos

  handleFileInput(arquivos: File, i: number) {
    this.solicitacaoForm.get('semDocumento').setValue(false);
    this.solicitacaoForm.get('semDocumento').enable();
    const randomId = Math.random().toString(36).substring(2);
    const ref = this.storage.ref(randomId);
    const task = ref.put(arquivos)
      .then(t => {
        t.ref.getDownloadURL().then(url => {
          console.log(this.orcamentosFormArray.at(i));
          this.documentosFormArray.at(i).get('endereco').setValue(url);
          this.documentosFormArray.at(i).get('nome').setValue(arquivos.name);
        });
      })
      .catch(error => console.log(error));
  }

  handleAceite(arquivos: File) {
    const randomId = Math.random().toString(36).substring(2);
    const ref = this.storage.ref(randomId);
    const task = ref.put(arquivos)
      .then(t => {
        t.ref.getDownloadURL().then(url => {
          this.solicitacaoForm.get('aceite').setValue(url);
        });
      })
      .catch(error => console.log(error));
  }

  handleOrcamentoInput(arquivos: File, i: number) {
    const randomId = Math.random().toString(36).substring(2);
    const ref = this.storage.ref(randomId);
    const task = ref.put(arquivos)
      .then(t => {
        const name = arquivos.name
        console.log(name)
        t.ref.getDownloadURL().then(url => {
          const formItem = this.cotacoesFormArray.at(i);
          console.log(formItem);
          formItem.get('endereco').setValue(url);
          formItem.get('nome').setValue(name);
        });
      })
      .catch(error => console.log(error));
  }

  // Adaptação da moeda
  changeMoeda(idMoeda: string) {
    this.moedas.findIndex((moedaSelecionada) => {
      if (moedaSelecionada.id === idMoeda) {
        this.simboloMoeda = moedaSelecionada.simbolo;
        return true;
      }
    });
    this.moedaOption = { prefix: this.simboloMoeda, thousands: '.', decimal: ',' };
    this.moedaOption = { ...this.moedaOption };
  }

  onMotivoChange() {
    if (this.solicitacaoForm.get('motivo').value === 'kAiMnJvYm4BBj3DoffOw') {
      this.showMotivo = true;
      this.solicitacaoForm.get('detalheMotivo').setValidators(Validators.required);
    } else {
      this.showMotivo = false;
      this.solicitacaoForm.get('detalheMotivo').setValidators([]);
    }
    this.solicitacaoForm.get('detalheMotivo').updateValueAndValidity();
  }

  changeType() {
    if (this.solicitacaoForm.get('tipoDeMaterial').value === 'RAS') {
      this.solicitacaoForm.get('reembolso').setValidators(Validators.required);
      this.solicitacaoForm.get('descProjeto').setValidators(Validators.required);
      this.solicitacaoForm.get('justificativa').setValidators(Validators.required);
      this.solicitacaoForm.get('categoriaBudget').setValidators(Validators.required);
      this.solicitacaoForm.get('cliente').setValidators(Validators.required);
      this.solicitacaoForm.get('programa').setValidators(Validators.required);
      this.solicitacaoForm.get('inicioProjeto').setValidators(Validators.required);
      this.solicitacaoForm.get('ppap').setValidators(Validators.required);
      this.solicitacaoForm.get('sop').setValidators(Validators.required);
    } else {
      this.solicitacaoForm.get('reembolso').setValidators(null);
      this.solicitacaoForm.get('descProjeto').setValidators(null);
      this.solicitacaoForm.get('justificativa').setValidators(null);
      this.solicitacaoForm.get('categoriaBudget').setValidators(null);
      this.solicitacaoForm.get('cliente').setValidators(null);
      this.solicitacaoForm.get('programa').setValidators(null);
      this.solicitacaoForm.get('inicioProjeto').setValidators(null);
      this.solicitacaoForm.get('ppap').setValidators(null);
      this.solicitacaoForm.get('sop').setValidators(null);
    }
  }

  updateProgram() {
    this.clienteService.getProgramas(this.solicitacaoForm.get('cliente').value).subscribe(cliente => {
      this.programas = cliente[0].programas;
    })
  }

  getTooltip() {
    if (this.solicitacao.alt === 'Recusada' || this.solicitacao.alt === 'Excluida') {
      if (this.solicitacao.alt === 'Recusada') {
        console.log(this.motivoString)
        if (this.motivoString === 'Outros') {
          return 'Recusada: ' + this.solicitacao.detalheMotivo
        } else {
          return 'Recusada: ' + this.motivoString
        }
      } else {
        return 'Excluída: ' + this.solicitacao.detalheMotivo
      }
    } else {
      return this.solicitacao.alt
    }
  }

  /*displayFn(fornecedor): string {
    console.log(this)
    /*const forn: Fornecedor[] = fornecedores.filter(f => {
      if (f.id === fornecedor) {
        return f;
      }
    })
    return 'teste'
  }*/

  get displayFn() {
    return (val) => {
      const forn: Fornecedor[] = this.fornecedores.filter(f => {
        if (f.id === val) {
          return f;
        }
      })
      return forn.length > 0 ? forn[0].nome : ''
    }
  }

}
