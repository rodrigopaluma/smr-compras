import { Component, OnInit, ElementRef, ViewChild, AfterViewInit, AfterContentInit, AfterViewChecked, ChangeDetectorRef } from '@angular/core';
import { SolicitacaoService, Solicitacao } from './solicitacao.service';
import { UsuarioService } from '../usuario/usuario.service';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-solicitacoes-list',
  templateUrl: './solicitacoes-list.component.html',
  styleUrls: ['./solicitacoes-list.component.scss']
})
export class SolicitacoesListComponent implements OnInit, AfterViewChecked {
  rows = [];
  selected = [];
  temp = [];
  searchValue: string = null;
  isSearchActive = false;
  isToolbarActive = false;
  itemsSelected = '';
  itemCount = 0;
  usuario = this.usuarioService.getCurrentUser();
  personalFilter = JSON.parse(localStorage.getItem('comprasFilter')) || null;

  showRecusadas = false;
  showFila = true;
  showCotacao = true;
  showNegociado = true;
  showPO = true;
  showRecebido = false;
  showExcluida = false;
  original = [];
  motivoDelete = '';
  vm = this;
  showMotivo = false;
  algoliaFila: ElementRef;
  runAgain = true;
  first = true
  indexName = 'smr-compras'

  @ViewChild(MatSort) sort: MatSort;

  toDelete: Solicitacao = <Solicitacao>{};


  options = {
    fieldSeparator: ',',
    quoteStrings: '"',
    decimalseparator: '.',
    showLabels: true,
    headers: ['Número', 'Categoria', 'Tipo', 'Solicitante', 'Departamento', 'Inclusão', 'Alteração', 'Status', 'Comprador'],
    showTitle: false,
    title: '',
    useBom: false,
    removeNewLines: true,
    keys: ['approved', 'age', 'name']
  };

  constructor(
    private service: SolicitacaoService,
    private toastr: ToastrService,
    private usuarioService: UsuarioService,
    private router: Router,
    private cdRef: ChangeDetectorRef
  ) {

  }

  updateFilter2(event) {
    const val = event.target.value.toLowerCase();
    // filter our data
    const temp = this.original.filter(function (d) {
      return d.numero.indexOf(val) !== -1 || !val;
    });
    // update the rows
    this.rows = temp;
    // Whenever the filter changes, always go back to the first page
    //  this.table.offset = 0;
  }
  ngAfterViewChecked() {
    // Ajuste do tamanho do input
    const divResults: HTMLCollectionOf<Element> = document.getElementsByClassName('results');
    const inputBusca: HTMLCollectionOf<Element> = document.getElementsByClassName('ais-SearchBox-input');
    inputBusca[0].setAttribute('style', 'width:' + (divResults[0].scrollWidth - 22) + 'px')

    // Filtros
    const labels: HTMLCollectionOf<Element> = document.getElementsByClassName('ais-RefinementList-label');
    if (labels) {
      for (let i = 0; i < labels.length; i++) {
        const item = labels.item(i).firstChild as HTMLInputElement
        if (item.value === 'Em Fila') {
          labels.item(i).classList.add('fila');
        } else if (item.value === 'Em Cotação') {
          labels.item(i).classList.add('cotacao');
        } else if (item.value === 'Excluida') {
          labels.item(i).classList.add('excluida');
        } else if (item.value === 'Recusada') {
          labels.item(i).classList.add('recusada');
        } else if (item.value === 'PO Emitido') {
          labels.item(i).classList.add('emitido');
        } else if (item.value === 'Recebido') {
          labels.item(i).classList.add('recebido');
        } else if (item.value === 'Negociado') {
          labels.item(i).classList.add('negociado');
        }
      }
      if (this.first) {
        this.applyFilter()
      }
    }
  }

  applyFilter() {
    // console.log('Filtro de cliques')
    const controls: HTMLCollectionOf<Element> = document.getElementsByClassName('ais-RefinementList-checkbox');
    const filtros = JSON.parse(localStorage.getItem('comprasFilter'));
    const user = JSON.parse(sessionStorage.getItem('currentUser'));
    for (let i = 0; i < controls.length; i++) {
      const item = controls.item(i) as HTMLInputElement
      if (item.value === 'Em Fila' && filtros.showFila && item.checked !== filtros.showFila) {
        // console.log('Fila clicado');
        this.first = false
        item.click();
        this.cdRef.detectChanges();
      } else if (item.value === 'Em Cotação' && filtros.showCotacao && item.checked !== filtros.showCotacao) {
        // console.log('Cotação clicado');
        this.first = false
        item.click();
        this.cdRef.detectChanges();
      } else if (item.value === 'Excluida' && filtros.showExcluida && item.checked !== filtros.showExcluida) {
        // console.log('Excluida clicado', item.checked, filtros.showExcluida);
        this.first = false
        item.click();
        this.cdRef.detectChanges();
      } else if (item.value === 'Negociado' && filtros.showNegociado && item.checked !== filtros.showNegociado) {
        // console.log('Negociado clicado');
        this.first = false
        item.click();
        this.cdRef.detectChanges();
      } else if (item.value === 'PO Emitido' && filtros.showPO && item.checked !== filtros.showPO) {
        // console.log('PO clicado');
        this.first = false
        item.click();
        this.cdRef.detectChanges();
      } else if (item.value === 'Recebido' && filtros.showRecebido && item.checked !== filtros.showRecebido) {
        // console.log('Recebido clicado');
        this.first = false
        item.click();
        this.cdRef.detectChanges();
      } else if (item.value === 'Recusada' && filtros.showRecusadas && item.checked !== filtros.showRecusadas) {
        // console.log('Recusado clicado');
        this.first = false
        item.click();
        this.cdRef.detectChanges();
      }
      if (item.value === user.departamento && user.role === 'normal') {
        this.first = false
        item.click();
        this.cdRef.detectChanges();
      }
    }
  }

  updateFilter(item) {
    const stateAIS = { ...item };
    let filters = JSON.parse(localStorage.getItem('comprasFilter')) || [];
    stateAIS.disjunctiveFacetsRefinements && stateAIS.disjunctiveFacetsRefinements.status ? filters = stateAIS.disjunctiveFacetsRefinements.status : filters = filters;
    if (filters.length > 0) {
      const personalFilter: any = {};
      filters.map(item => {
        if (item === 'Em Fila') {
          personalFilter.showFila = true
        } else if (item === 'Em Cotação') {
          personalFilter.showCotacao = true
        } else if (item === 'Excluida') {
          personalFilter.showExcluida = true
        } else if (item === 'Negociado') {
          personalFilter.showNegociado = true
        } else if (item === 'PO Emitido') {
          personalFilter.showPO = true
        } else if (item === 'Recebido') {
          personalFilter.showRecebido = true
        } else if (item === 'Recusada') {
          personalFilter.showRecusadas = true
        }
      })
      // console.log('Load Filter', filters, personalFilter)
      localStorage.setItem('comprasFilter', JSON.stringify(personalFilter))
    } else {
      this.first = false;
    }
  }

  ngOnInit() {
    if (this.personalFilter) {
      this.isSearchActive = true;
      this.showCotacao = this.personalFilter.showCotacao;
      this.showExcluida = this.personalFilter.showExcluida;
      this.showFila = this.personalFilter.showFila;
      this.showNegociado = this.personalFilter.showNegociado;
      this.showPO = this.personalFilter.showPO;
      this.showRecebido = this.personalFilter.showRecebido;
      this.showRecusadas = this.personalFilter.showRecusadas;
    }
    setTimeout(() => this.applyFilter(), 500);
  }

  onSelect({ selected }) {
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
    if (selected.length === 1) {
      this.isToolbarActive = true;
      this.itemCount = selected.length;
      this.itemsSelected = 'Item Selected';
    } else if (selected.length > 0) {
      this.isToolbarActive = true;
      this.itemCount = selected.length;
      this.itemsSelected = 'Items Selected';
    } else {
      this.isToolbarActive = false;
    }
  }
  triggerClose(event) {
    this.rows = this.original;
    this.showCotacao = this.showFila = this.showNegociado = this.showPO = true;
    this.updatePersonalFilter();
    this.searchValue = '';
    this.isSearchActive = !this.isSearchActive;
  }
  onActivate(event) {
    // console.log("Activate Event", event);
  }

  add() {
    this.selected.push(this.rows[1], this.rows[3]);
  }

  update() {
    this.selected = [this.rows[1], this.rows[3]];
  }

  remove() {
    this.selected = [];
  }

  makeReport() {
    // tslint:disable-next-line:no-unused-expression
    const data = [];
    this.rows.map(sol => {
      data.push({
        Número: sol.numero,
        Categoria: sol.material,
        Tipo: sol.tipoDeMaterial,
        Solicitante: sol.solicitante,
        Departamento: sol.departamento,
        Inclusão: sol.inclusao.toDate().toISOString().substr(0, 10).split('-').reverse().join('/'),
        Alteração: sol.alteracao.toDate().toISOString().substr(0, 10).split('-').reverse().join('/'),
        Status: sol.alt,
        Comprador: sol.comprador
      })
    });
    // tslint:disable-next-line:no-unused-expression
    new Angular2Csv(data, 'Compras', this.options);
  }

  filtrar() {
    this.rows = this.original.filter(sol => {
      if (sol.alt === 'Recusada' && this.showRecusadas) { return sol }
      if (sol.alt === 'Em Fila' && this.showFila) { return sol }
      if (sol.alt === 'Em Cotação' && this.showCotacao) { return sol }
      if (sol.alt === 'Negociado' && this.showNegociado) { return sol }
      if (sol.alt === 'PO Emitido' && this.showPO) { return sol }
      if (sol.alt === 'Recebido' && this.showRecebido) { return sol }
      if (sol.alt === 'Excluida' && this.showExcluida) { return sol }
    });
    this.updatePersonalFilter();
  }

  filtroAlgolia(items) {
    const filteredIds = [];
    const filteredItems = items.filter(item => {
      filteredIds.push(item.objectID);
      return getRowClass(item);
    });
    localStorage.setItem('itensComprasFilter', JSON.stringify(filteredIds));
    return filteredItems

    function getRowClass(row) {
      const hoje = moment();
      let alvo;
      if (row.prazo) {
        alvo = moment(row.prazo);
      } else {
        row.tipoMaterial === 'RAS' ? alvo = addWeekdays(row.dataInclusao, 15) : alvo = addWeekdays(row.dataInclusao, 5);
      }
      if (hoje > alvo && (row.status === 'Em Fila' || row.status === 'Em Cotação')) {
        row.foraPrazo = true
      }
      return row;
      function addWeekdays(date, days) {
        date = moment(date, 'DD/MM/YYYY'); // use a clone
        while (days > 0) {
          date = date.add(1, 'days');
          // decrease "days" only if it's a weekday.
          if (date.isoWeekday() !== 6 && date.isoWeekday() !== 7) {
            days -= 1;
          }
        }
        return date;
      }
    }
  }

  updatePersonalFilter() {
    !this.personalFilter ? this.personalFilter = {} : this.personalFilter = this.personalFilter
    this.personalFilter.showCotacao = this.showCotacao;
    this.personalFilter.showExcluida = this.showExcluida;
    this.personalFilter.showFila = this.showFila;
    this.personalFilter.showNegociado = this.showNegociado;
    this.personalFilter.showPO = this.showPO;
    this.personalFilter.showRecebido = this.showRecebido;
    this.personalFilter.showRecusadas = this.showRecusadas;
    localStorage.setItem('comprasFilter', JSON.stringify(this.personalFilter));
    const itensComprasFilter: string[] = [];
    this.rows.map(r => {
      itensComprasFilter.push(r.id);
    })
    localStorage.setItem('itensComprasFilter', JSON.stringify(itensComprasFilter));
    // console.log('Filtro Atualizado')
  }

  confirmDelete() {
    this.toDelete.alt = 'Excluida';
    this.toDelete.img = 'lixeira.png'
    this.toDelete.detalheMotivo = this.motivoDelete;
    this.service.updateSolicitacao(this.toDelete)
      .then(() => {
        this.toDelete = <Solicitacao>{}
        this.toastr.success('Solicitação Excluída!', 'Sucesso').onHidden.subscribe(() => {
          this.router.navigate(['/solicitacao']);
        });
      }).catch(() => {
        this.toastr.error('Ocorreu um erro, tente novamente mais tarde.', 'Erro');
      });
  }

  delete(solId, modal) {
    this.service.getSolicitacao(solId).subscribe(sol => {
      this.toDelete = sol;
      modal.show();
    })
  }

  resolveImagemStatus(status: string) {
    if (status === 'Em Fila') {
      return '/assets/img/em-fila.png'
    } else if (status === 'Em Cotação') {
      return '/assets/img/cotacao.png'
    } else if (status === 'Negociado') {
      return '/assets/img/negociado.png'
    } else if (status === 'PO Emitido') {
      return '/assets/img/po-emitido.png'
    } else if (status === 'Recebido') {
      return '/assets/img/recebido.png'
    } else if (status === 'Excluida') {
      return '/assets/img/lixeira.png'
    } else if (status === 'Recusada') {
      return '/assets/img/recusada.png'
    }
  }

  orderStatus(items) {
    let novo = [];
    items.map(item => {
      if (item.value === 'Em Fila') {
        novo[0] = item
      } else if (item.value === 'Em Cotação') {
        novo[1] = item
      } else if (item.value === 'Negociado') {
        novo[2] = item
      } else if (item.value === 'PO Emitido') {
        novo[3] = item
      } else if (item.value === 'Recebido') {
        novo[4] = item
      } else if (item.value === 'Recusada') {
        novo[5] = item
      } else if (item.value === 'Excluida') {
        novo[6] = item
      }
    })
    novo = novo.filter(item => {
      if (item) {
        return item
      }
    })
    return novo;
  }

  changeOrder(name) {
    const selectSort: HTMLCollectionOf<Element> = document.getElementsByClassName('ais-SortBy-option');
    for (let i = 0; i < selectSort.length; i++) {
      const option = selectSort[i] as HTMLOptionElement
      if (option.label === name) {
        option.selected = true
        this.simulateClick(option)
      }
    }
  }
  simulateClick(item) {
    item.dispatchEvent(new PointerEvent('pointerdown', { bubbles: true }));
    item.dispatchEvent(new MouseEvent('mousedown', { bubbles: true }));
    item.dispatchEvent(new PointerEvent('pointerup', { bubbles: true }));
    item.dispatchEvent(new MouseEvent('mouseup', { bubbles: true }));
    item.dispatchEvent(new MouseEvent('mouseout', { bubbles: true }));
    item.dispatchEvent(new MouseEvent('click', { bubbles: true }));
    item.dispatchEvent(new Event('change', { bubbles: true }));

    return true;
  }

}
