import { Component, OnInit, Input } from '@angular/core';
import { TipoMaterialService } from './tipo-material.service';
import { ToastrService } from '../../../../node_modules/ngx-toastr';

@Component({
  selector: 'app-tipo-material',
  templateUrl: './tipo-material.component.html',
  styleUrls: ['./tipo-material.component.scss']
})
export class TipoMaterialComponent implements OnInit {
  @Input() mode = 'normal';
  rows = [];
  selected = [];
  temp = [];
  searchValue: string = null;
  isSearchActive = false;
  isToolbarActive = false;
  itemsSelected = '';
  itemCount = 0;
  editing = {};


  constructor(private service: TipoMaterialService, private toast: ToastrService) { }

  ngOnInit() {
    this.service.getTiposMateriais().subscribe(tiposMateriais => {
      this.rows = tiposMateriais;
    });
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    const temp = this.temp.filter(function (d) {
      return d.productTitle.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.rows = temp;
  }

  onSelect({ selected }) {
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
    if (selected.length === 1) {
      this.isToolbarActive = true;
      this.itemCount = selected.length;
      this.itemsSelected = 'Item Selected';
    } else if (selected.length > 0) {
      this.isToolbarActive = true;
      this.itemCount = selected.length;
      this.itemsSelected = 'Items Selected';
    } else {
      this.isToolbarActive = false;
    }
  }

  triggerClose(event) {
    this.rows = this.temp;
    this.searchValue = '';
    this.isSearchActive = !this.isSearchActive;
  }

  onActivate(event) {

  }

  add() {
    this.rows.push({});
    this.editing[(this.rows.length - 1) + '-tipo'] = true;
    this.rows = [...this.rows];
  }


  remove() {
    this.selected.map(obj => {
      this.service.removeTipoMaterial(obj.id).then(() => {
        this.toast.success('Categoria excluída com sucesso', 'Sucesso');
      });
    });
    this.isToolbarActive = false;
    this.selected = [];
  }

  updateValue(event, cell, rowIndex) {
    this.editing[rowIndex + '-' + cell] = false;
    this.rows[rowIndex][cell] = event.target.value;
    const categoria = this.rows[rowIndex];
    if (categoria.id) {
      this.service.updateTipoMaterial(categoria).then(() => {
        this.toast.success('Categoria atualizada com sucesso.', 'Sucesso');
      });
    } else {
      if (categoria.tipo) {
        this.service.addTipoMaterial(categoria).then(() => {
          this.toast.success('Categoria incluída com sucesso.', 'Sucesso');
        });
      }
    }
    this.rows = [...this.rows];
  }


}
