import { NgModule } from '@angular/core';
import { TipoMaterialComponent } from './tipo-material.component';
import { SharedModule } from '../../shared/shared.module';
import { RouterModule } from '@angular/router';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

const ROUTE = [
  { path: 'list', component: TipoMaterialComponent },
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(ROUTE),
    NgxDatatableModule
  ],
  declarations: [TipoMaterialComponent],
  exports: [TipoMaterialComponent]
})
export class TipoMaterialModule { }
