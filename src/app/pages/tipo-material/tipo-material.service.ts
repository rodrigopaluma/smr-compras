import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

export interface TipoMaterial {
  id: string,
  tipo: string;
}
@Injectable()
export class TipoMaterialService {

  constructor(private db: AngularFirestore) { }

  getTiposMateriais(): Observable<TipoMaterial[]> {
    return this.db.collection<TipoMaterial>('tipo_material').valueChanges();
  }

  getTipoMaterial(id: string): Observable<TipoMaterial> {
    return this.db.collection('tipo_material').doc<TipoMaterial>(id).valueChanges();
  }

  addTipoMaterial(tipoMaterial: TipoMaterial): Promise<boolean> {
    tipoMaterial.id = this.db.createId();
    // tslint:disable-next-line:max-line-length
    return this.db.collection('tipo_material').doc(tipoMaterial.id).set(tipoMaterial).then(() => true).catch((error) => { console.log(error); return false});
  }

  updateTipoMaterial(tipoMaterial: TipoMaterial): Promise<boolean> {
    // tslint:disable-next-line:max-line-length
    return this.db.collection('tipo_material').doc(tipoMaterial.id).update(tipoMaterial).then(() => true).catch((error) => { console.log(error); return false});
  }

  removeTipoMaterial(id: string): Promise<boolean> {
    return this.db.collection('tipo_material').doc(id).delete().then(() => true).catch((error) => { console.log(error); return false});
  }
}
