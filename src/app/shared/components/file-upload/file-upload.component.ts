import { Component, OnInit, Input, forwardRef } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => FileUploadComponent),
    multi: true
  }]
})
export class FileUploadComponent implements OnInit {
  public _value;
  uploadPercent: Observable<number>;
  downloadURL: Observable<string>;

  @Input()
  disabled = false;

  constructor(private storage: AngularFireStorage) { }

  ngOnInit() { }

  get value() {
    const url = this._value || '';
    return url;
  }

  set value(val) {
    if (val) {
      this._value = val;
      this.propagateChange(this._value);
    }
  }

  onFileInput(files) {
    const file = files[0];
    const filePath = 'pco/';
    if (!this._value) {
      this._value = {}
    }
    this.value.name = file.name;
    const randomId = Math.random().toString(36).substring(2);
    const fileRef = this.storage.ref(randomId);
    const task = fileRef.put(file).then(t => {
      t.ref.getDownloadURL().then(url => {
        this.value.downloadURL = url;
      });
    })

      this.propagateChange(this._value);
  }



  /* Takes the value  */
  writeValue(value: any) {
    if (value !== undefined) {
      this.value = value;
      this.propagateChange(this.value);
    }
  }

  propagateChange = (_: any) => { };

  registerOnChange(fn) {
    this.propagateChange = fn;
  }

  registerOnTouched() { }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }
}
