import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from '../../../../../node_modules/rxjs';
import { map } from '../../../../../node_modules/rxjs-compat/operator/map';

export interface Configuracao {
  ano: number;
  index: number;
}
@Injectable()
export class ConfiguracaoService {

  constructor(private db: AngularFirestore) { }

  getConfiguracoes(): Observable<Configuracao> {
    const year = new Date().getFullYear();
    return this.db.doc<Configuracao>(`configuracao/${year}`).valueChanges();
  }

  updateConfiguracoes(old: number) {
    const year = new Date().getFullYear().toString();
    this.db.collection('configuracao').doc(year).update({index: old + 1});
  }

}
