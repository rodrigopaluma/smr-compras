import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase';

import 'rxjs/add/operator/take';
import { BehaviorSubject } from 'rxjs/BehaviorSubject'
import { UsuarioService } from '../../../pages/usuario/usuario.service';

@Injectable()
export class MessagingService {

    messaging = firebase.messaging()
    currentMessage = new BehaviorSubject(null)

    constructor(
        private afAuth: AngularFireAuth,
        private usuarioService: UsuarioService
    ) { }


    updateToken(token) {
        this.afAuth.authState.take(1).subscribe(user => {
            if (!user) { return };
            const userId = user.uid;
            const sub = this.usuarioService.getUsuario(userId).subscribe(usuario => {
                usuario.token = token;
                this.usuarioService.updateUsuario(usuario).then(() => {
                    sub.unsubscribe();
                });
            })
        })
    }

    getPermission() {
        this.messaging.requestPermission()
            .then(() => {
                return this.messaging.getToken()
            })
            .then(token => {
                this.updateToken(token)
            })
            .catch((err) => {
                console.log('Unable to get permission to notify.', err);
            });
    }

    receiveMessage() {
        this.messaging.onMessage((payload) => {
            this.currentMessage.next(payload)
        });

    }

    deleteMessage() {
        this.currentMessage.next(null);
    }
}
