// import { Http, Headers, Response, Jsonp, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';

@Injectable()
export class AcentosService {

  remover(s: string): string {
    s = s.toString().toLowerCase()
      .replace(/\s+/g, '-')
      .replace(/\-\-+/g, '-')
      .replace(/^-+/, '')
      .replace(/\:|\;|\,|\./g, '')
      .replace(/\$/g, 'S')
      .replace(/-+$/, '');

    let novaString = '';
    for (let i = 0; i < s.length; i++) {
      const letra = s[i].replace(/[^a-z0-9 ]/, '');
      novaString = novaString + letra;
    }
    s = novaString;

    const map = {
      'â': 'a',
      'Â': 'A',
      'à': 'a',
      'À': 'A',
      'á': 'a',
      'Á': 'A',
      'ã': 'a',
      'Ã': 'A',
      'ê': 'e',
      'Ê': 'E',
      'è': 'e',
      'È': 'E',
      'é': 'e',
      'É': 'E',
      'î': 'i',
      'Î': 'I',
      'ì': 'i',
      'Ì': 'I',
      'í': 'i',
      'Í': 'I',
      'õ': 'o',
      'Õ': 'O',
      'ô': 'o',
      'Ô': 'O',
      'ò': 'o',
      'Ò': 'O',
      'ó': 'o',
      'Ó': 'O',
      'ü': 'u',
      'Ü': 'U',
      'û': 'u',
      'Û': 'U',
      'ú': 'u',
      'Ú': 'U',
      'ù': 'u',
      'Ù': 'U',
      'ç': 'c',
      'Ç': 'C',
      '$': 'S',
      '?': '',
      '!': '',
      '%': '',
      '&': '',
      '*': '',
    };
    return s.replace(/[\W\[\] ]/g, function (a) { return map[a] || a; }).replace(/-+$/g, '');
  }
}
