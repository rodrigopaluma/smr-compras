import { NgModule, Optional, SkipSelf } from '@angular/core';

import { ConfigService } from './config/config.service';
import { PreloaderService } from './preloader/preloader.service';
import { SpinnerService } from './spinner/spinner.service';
import { DataService } from './data/data.service';
import { ThemesService } from './themes/themes.service';
import { MoedasService } from '../../pages/moedas/moedas.service';
import { TipoMaterialService } from '../../pages/tipo-material/tipo-material.service';
import { UnidadeMedidaService } from '../../pages/unidade-medida/unidade-medida.service';
import { MotivoRecusaService } from '../../pages/motivo-recusa/motivo-recusa.service';
import { FornecedorService } from '../../pages/fornecedores/fornecedores.service';
import { UsuarioService } from '../../pages/usuario/usuario.service';
import { DepartamentosService } from '../../pages/departamentos/departamentos.service';
import { ClienteService } from '../../pages/cliente/cliente.service';
import { CategoriaDoBudgetService } from '../../pages/categoria-do-budget/categoria-do-budget.service';
import { SolicitacaoService } from '../../pages/solicitacao/solicitacao.service';
import { ConfiguracaoService } from './configuracao/configuracao.service';
import { LoggedInGuard } from '../security/loggedin.guard';
import { AcentosService } from './acentos/acentos.service';
import { MessagingService } from './messaging/messaging.service';
import { ProgramaService } from '../../pages/programa/programa.service';
import { ProcedimentosService } from '../../pages/procedimentos/procedimentos.service';
import { ContratosService } from 'app/pages/contratos/contratos.service';

@NgModule({
	imports: [],
	providers: [
		ConfigService,
		ThemesService,
		PreloaderService,
		SpinnerService,
		DataService,
		MoedasService,
		TipoMaterialService,
		UnidadeMedidaService,
		MotivoRecusaService,
		FornecedorService,
		UsuarioService,
		DepartamentosService,
		CategoriaDoBudgetService,
		ClienteService,
		ProgramaService,
		SolicitacaoService,
		ConfiguracaoService,
		LoggedInGuard,
		AcentosService,
		MessagingService,
    ProcedimentosService,
    ContratosService
	],
	declarations: [],
	exports: []
})
export class ServicesModule {
	constructor(
		@Optional()
		@SkipSelf()
		parentModule: ServicesModule
	) { }
}
