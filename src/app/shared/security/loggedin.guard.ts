import { CanLoad, Route, Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UsuarioService } from '../../pages/usuario/usuario.service';
import { Injectable } from '../../../../node_modules/@angular/core';
import { ToastrService } from '../../../../node_modules/ngx-toastr';

@Injectable()
export class LoggedInGuard implements CanActivate {

    constructor(
        private service: UsuarioService,
        private toast: ToastrService,
        private router: Router
    ) {

    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        const usuario = this.service.getCurrentUser();
        if (!usuario.id) {
            this.toast.error('Você não está logado, vamos redirecioná-lo para o login.');
            this.router.navigate(['authentication/login']);
            return false;
        } else if (usuario.role === 'administrador') {
            return true;
        } else {
            const rota = route.routeConfig.path;
            // console.log(rota);
            if (rota === 'fornecedores' || rota === 'solicitacao' || rota === 'categorias' || rota === `:id` || rota === `usuario` || rota === `contratos`) {
                return true;
            } else {
                this.toast.error('Você não tem autorização para acessar. Fale com um administrador.');
                return false;
            }
        }
    }

}
