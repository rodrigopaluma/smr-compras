import {BrowserModule, Title} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ResponsiveModule} from 'ng2-responsive';
import {AppComponent} from './app.component';
import {AppState, InternalStateType} from './app.service';
import {GlobalState} from './app.state';
import {ServicesModule} from './shared/services/services.module';
import {SharedModule} from './shared/shared.module';
import {AppRoutingModule} from './app.routing';
import {LocalStorageTodoService} from './shared/services/localstorage/localstorage-todo.service';
import {HttpClientModule} from '@angular/common/http';
import { environment } from 'environments/environment';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { ToastrModule } from 'ngx-toastr';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { NgAisModule } from 'angular-instantsearch';
// import { InMemoryTodoService } from "./shared/services/inMemory/in-memory-todo.service";
// Application wide providers
const APP_PROVIDERS = [AppState, GlobalState, Title, LocalStorageTodoService];

// tslint:disable-next-line:interface-over-type-literal
export type StoreType = {
    state: InternalStateType;
    restoreInputValues: () => void;
    disposeOldHosts: () => void;
};

@NgModule({
    declarations: [AppComponent],
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        ServicesModule,
        ResponsiveModule,
        SharedModule.forRoot(),
        AngularFireModule.initializeApp(environment.firebase),
        AngularFirestoreModule,
        AngularFireStorageModule,
        AngularFireAuthModule,
        AppRoutingModule,
        ToastrModule.forRoot(),
        NgAisModule.forRoot()
    ],
    providers: [APP_PROVIDERS],
    bootstrap: [AppComponent]
})
export class AppModule {
    constructor(public appState: AppState) {
    }
}
